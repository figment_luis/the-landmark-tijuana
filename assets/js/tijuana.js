// Tabs info depto
$("#datasheet-tabs a").click(function(e) {
    e.preventDefault()
    e.stopPropagation()
    $("#datasheet-tabs a").removeClass('active');
    $(this).toggleClass('active');
    let tipoDepto = $(this).data('depto')
    $("[data-infodepto]").fadeOut(400, function () {
        setTimeout(function() {
            $(`[data-infoDepto=${tipoDepto}]`).addClass('active').fadeIn()
        }, 380)
    })

})

// Funcionalidad submenú sticky y mobile
$("#navbar #submenu").on('click', function (e) {
    e.preventDefault()
    e.stopPropagation()
    $("#navbar .submenu").slideToggle();
    //$(this).toggleClass('active')
})

// Funcionalidad submenú header (banner)
$(".top-header #submenu").on('click', function (e) {
    e.preventDefault()
    e.stopPropagation()
    $(".top-header .submenu").slideToggle();
    //$(this).toggleClass('active')
})
// Cerrar submenú al hacer clic fuera del mismo
$("body").click(function () {
    $(".submenu").slideUp();
   // $(this).removeClass('active')
})

// Slide - Sección Live
$('#slick-live').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: true,
    //fade: true,
    //cssEase: 'linear',
    speed: 500,
})


// Lightbox: Mover botón de cerrar en la parte superior
$(function() {
    if($('.lightbox').length) {
        var lbOuterContainer = $('.lightbox').find('.lb-outerContainer');
        var lbDataContainer = $('.lightbox').find('.lb-dataContainer');
        $(lbOuterContainer).before($(lbDataContainer));
    }
});
// Lightbox: Configuración modales
lightbox.option({
    'disableScrolling': true,
    'showImageNumberLabel': false,
})


// Formulario de registro
$('#contactForm').submit(function (e) {
    e.preventDefault()
    e.stopPropagation()

    $('#btnSubmit').prop('disabled', true)
    $('#loading').show()

    const myForm = $(this)[0];
    let formData = new FormData(myForm);

    $.ajax({
        url: url + '/registro',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        headers: {'X-Requested-With': 'XMLHttpRequest'},
        dataType: 'json',
    }).done(function(data, textStatus, jqXHR) {
        clearValidationErrors()
        switch (data.code) {
            case 200:
                myForm.reset();
                Swal.fire({
                    title: 'Proceso terminado',
                    text: data.message,
                    icon: 'success',
                    confirmButtonText: 'CERRAR',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                })
                break;
            case 406:
                $.each(data.errors, function(key, value) {
                    $(`*[name='${key}']`).addClass('is-invalid').parent().append(`<div class="invalid-feedback">${value}</div>`)
                })
                break;
            case 500:
                Swal.fire({
                    title: 'Lo sentimos',
                    text: data.message,
                    icon: 'error',
                    confirmButtonText: 'CERRAR',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                })
                break
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        clearValidationErrors()
        Swal.fire({
            title: 'Lo sentimos',
            text: 'Se detectó un error desconocido en el sistema, favor de intentar más tarde.',
            icon: 'error',
            confirmButtonText: 'CERRAR',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
        })
    }).always(function() {
        $('#btnSubmit').prop('disabled', false)
        $('#loading').hide()
    })

})


// Limpiar mensajes de validación
function clearValidationErrors()
{
    $('.invalid-feedback').hide();
    $('.is-invalid').removeClass('is-invalid')
}
