window.onscroll = function() {scrollFunction()};

// Navbar
function scrollFunction() {
    if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
        document.getElementById("navbar").style.top = "0";
        // Ocultar opciones de submenú - sección banner
        $(".top-header .submenu").hide();
    } else {
        document.getElementById("navbar").style.top = "-75px";
        $(".navbar-collapse").removeClass("show");
        // Ocultar las opciones de submenú - sección menu sticky
        $("#navbar .submenu").hide();
    }
}

// Search
$(document).ready(function(){

    // Descomentar para activar la función de búsqueda en el sitio
    // load_data();

    function load_data(query){
        var urlActual = window.location;
        $.ajax({
            url: url+'/search',
            method: "POST",
            data:{query:query},
            success:function(data){
                $('#search_result').html(data);
            }
        })
    }

    $('#search_text').keyup(function(){
        var search = $(this).val();
        if(search != ''){
            load_data(search);
        }
        else{
            load_data();
        }
    });

    //Delete Empty Filters
    
    let letters = ['A','B','C','D','E','F','G','H','I','J','K','L','Ñ','N','M','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    
    letters.forEach(function(letter) {
        if (! $('#stores .item-store').filter('.letter-'+letter).length) {
            $('#'+letter+'_filter').addClass('d-none');
        }
    });

    //Start Hidden Toggle

    $(".filter-content").hide();

    //Start Hidden No Stores MEssage

    $("#notFindStores").hide();

    //Hide duplicates
    hideDuplicates(); 




    /********************************************************************************************/
    /*Subcategory Block*/
    /********************************************************************************************/
    let site_url = $(location).attr('href'); 
    let filter_init = site_url.split('/').reverse()[0];
    let filter_flag = site_url.split('/').reverse()[1];
    let showItems = false;
    var lastItem = "";


    if (filter_flag == "filtro"){ //Si el documento esta listo y encuentro la bandera subcategoria, ejecuto los siguientes cambios

        $('html, body').animate({scrollTop: $('#filtros').offset().top }, 'slow');

        $(".filter-content").show();
        $("#filter-all").removeClass('active');
        $(".letter-filter span").removeClass('active');
        $(".filter-content li a").removeClass('active');
        $("#"+filter_init).addClass('active');
        $('#stores .item-store').hide('1000');
        hideDuplicates(); 


        $('#stores .item-store').each(function (){
            if ($(this).find("a h4").hasClass(filter_init)  && lastItem != $(this).find("a h4").text() ){
                $(this).show('1000');
                showItems = true;
                lastItem = $(this).find("a h4").text();
            }
        });
        if (showItems == false){
            $("#notFindStores").show();
        }
    }
    /********************************************************************************************/

});

// Animation icon concierge
setInterval(() => {
    $(".header-concierge img").addClass("animate__animated animate__tada");
    setTimeout(() => {
        $(".header-concierge img").removeClass("animate__animated animate__tada");
    }, 2000);  
}, 3000);


function hideDuplicates(){
    var lastItem = "first";
    $('#stores .item-store').each(function (){
        if ($(this).find("a h4").text() == lastItem){
            $(this).hide();
        };
        lastItem = $(this).find("a h4").text();
    });
};


// Filter Stores

$("#filter-all").click(function () { 
    $("#filter-all").addClass('active');
    $(".letter-filter span").removeClass('active');
    $(".filter-content li a").removeClass('active');
    $('#stores .item-store').show('1000');
    hideDuplicates();
    $("#notFindStores").hide();
});


$(".letter-filter span").click(function(){
    var value = $(this).text();
    
    $("#filter-all").removeClass('active');
    $(".letter-filter span").removeClass('active');
    $(".filter-content li a").removeClass('active');
    $(this).addClass('active');
    

    $('#stores .item-store').not('.letter-'+value).hide('1000');
    $('#stores .item-store').filter('.letter-'+value).show('1000');

    if ($('#stores .item-store').filter('.letter-'+value) > 0) {
        $(this).addClass('d-none');
    }
    hideDuplicates();
    $("#notFindStores").hide();

});

// Filter SubCategories

$(".filterSubCategories span#btn-filter").click(function(){
    $(".filter-content").toggle(550);
    if ($(".filterSubCategories span.btn-filter i").hasClass("fa-angle-down")){
        $(".filterSubCategories span.btn-filter i").removeClass("fa-angle-down").addClass("fa-angle-up");
    }
    else if ($(".filterSubCategories span.btn-filter i").hasClass("fa-angle-up")){
        $(".filterSubCategories span.btn-filter i").removeClass("fa-angle-up").addClass("fa-angle-down");
    }
});


$(".filter-content li a").click(function(){
    var value = $(this).attr("id");
    var showItems = false;
    var lastItem = "";

    $("#notFindStores").hide();

    $("#filter-all").removeClass('active');
    $(".letter-filter span").removeClass('active');
    $(".filter-content li a").removeClass('active');
    $(this).addClass('active');
    $('#stores .item-store').hide('1000');
    $('#stores .item-store').each(function (){
        if ($(this).find(" a h4").hasClass(value) && lastItem != $(this).find("a h4").text()){
            $(this).show('1000');
            showItems = true;
            lastItem = $(this).find("a h4").text();
        }
    });
    if (showItems == false){
        $("#notFindStores").show();
    }

});



//popovers

$(function () {
    $('[data-toggle="popover"]').popover()
  })
 

//Scroll-down arrow

$(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('#info-page').offset().top }, 'slow');
      return false;
    });
});


//Scroll-down contact info

$(function() {
    $('.visitanos').click (function() {
      $('html, body').animate({scrollTop: $('#mapa').offset().top }, 'slow');
      return false;
    });
});

//PopOver Dissmiss

$('.popover-dismiss').popover({
    trigger: 'focus'
})


//Newsletter

$("#newsletter-form").submit(function(e){
    e.preventDefault();
    console.log("funciona");
    $.ajax({
        type: "POST",
        url: url+"/saveOnNewsletter",
        data: $(this).serialize(),
        success: function(){
            $("#newsletter input").hide();
            $("#newsletter button").hide();
            $("#newsletter p").text("   POR SUSCRIBIRTE").prepend("<span class='text-signature'>Gracias</span>");
        },
        error: function(){

        }
    });

});
