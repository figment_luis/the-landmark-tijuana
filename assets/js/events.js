
//Funcion para cambiar la imagen en la sección eventos
function switchEventImage(imageUrl,num_img) {
  $(".event-image img").attr('src', imageUrl);
  $(".image-container a.galery-btn").attr("href", imageUrl);
  $(".image-container a").remove();


  for (let i = 1 ; i <= num_img ; i++){
    $(".image-container").append($("<a data-lightbox='galery-recents'></a>").attr("href", imageUrl.replace('1.jpg', i+'.jpg')));
  }
  $(".image-container a").first().addClass("galery-btn").text("Ver más fotos");
}


function recentsChange(imageUrl){
  $(".selector-rp").find(".event-active").removeClass("event-active");
  $(".sel-r").addClass("event-active");

  $(".recent-events").removeClass("d-none");
  $(".upcoming-events").addClass("d-none");
  $(".event").removeClass("event-active");
  $(".default").addClass("event-active");
  $(".event").find(".event-description.d-block").removeClass("d-block").addClass("d-none");
  $(".default").children(".event-description").removeClass("d-none").addClass("d-block");
  switchEventImage(imageUrl);

}

function upcomingsChange(imageUrl){
  $(".selector-rp").find(".event-active").removeClass("event-active");
  $(".sel-p").addClass("event-active");

  $(".recent-events").addClass("d-none");
  $(".upcoming-events").removeClass("d-none");
  $(".event").removeClass("event-active");
  $(".default").addClass("event-active");
  $(".event").find(".event-description.d-block").removeClass("d-block").addClass("d-none");
  $(".default").children(".event-description").removeClass("d-none").addClass("d-block");
  switchEventImage(imageUrl);
}


$(document).ready(function () {
  
  /*Esta funcion cambia la clase event-active en los eventos*/
  $(".event").on("click", function () {
    $(".event-selector").find(".event-active").removeClass("event-active");
    $(".event").find(".event-description.d-block").removeClass("d-block").addClass("d-none");
    $(this).addClass("event-active");
    $(this).children(".event-description").removeClass("d-none").addClass("d-block");
  });

  /*Inicializo la clase animated*/
  $(".animated").addClass("delay-1s");
  
});