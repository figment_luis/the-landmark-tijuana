<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Main');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override(function() {
    return view('pages/404');
});
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Page::index');

$routes->get('/departamentos', 'Page::departamentos');
$routes->get('/retail', 'Page::retail');
$routes->get('/oficinas', 'Page::oficinas');
$routes->get('/contacto', 'Page::contacto');

$routes->post('/registro', 'Page::registro');
$routes->get('/aviso-privacidad', 'Main::avisoprivacidad');

/*$routes->get('/gastronomia', 'Main::categories/gastronomia');
$routes->get('/shopping', 'Main::categories/shopping');
$routes->get('/entretenimiento', 'Main::categories/entretenimiento');
$routes->get('/servicios', 'Main::services');
$routes->get('/gastronomia/(:segment)', 'Main::categories/gastronomia/$1');
$routes->get('/shopping/(:segment)', 'Main::categories/shopping/$1');
$routes->get('/moda/(:segment)', 'Main::categories/shopping/$1');
$routes->get('/estilo-de-vida/(:segment)', 'Main::categories/shopping/$1');
$routes->get('/entretenimiento/(:segment)', 'Main::categories/entretenimiento/$1');
$routes->get('/servicios/(:segment)', 'Main::categories/servicios/$1');*/

/*$routes->post('/search', 'Main::search');
$routes->post('/search/(:segment)', 'Main::search/$1');
$routes->post('/saveOnNewsletter', 'Main::saveOnNewsletter');*/


/*$routes->get('/entretenimiento/(:segment)/(:segment)', 'Main::categories/$1/$2');
$routes->get('/shopping/(:segment)/(:segment)', 'Main::categories/$1/$2');*/

//$routes->get('/servicios/(:segment)/(:segment)', 'Main::categories/$1/$2');

/*$routes->get('/gastronomia/(:segment)/(:segment)', 'Main::categories/$1/$2');
$routes->get('/buenfin', 'Microsite::buenfin', ['as' => 'buenfin']);*/




/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
