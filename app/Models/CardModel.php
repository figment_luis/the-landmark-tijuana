<?php

namespace App\Models;

use CodeIgniter\Model;

class CardModel extends Model
{
    protected $table      = 'cards';
    protected $primaryKey = 'id';

    protected $returnType     = 'object';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

}