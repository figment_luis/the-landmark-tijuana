<?php namespace App\Models;

use CodeIgniter\Model;

class SubcategoriesModel extends Model
{
    protected $table      = 'subcategories';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['subcat_name'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    function get_subcategories_data(){
        $query = $this->query("SELECT sub.*, cat.name FROM categories as cat left join subcategories as sub on cat.id=sub.category_id");
        return $query->getResultArray();
    }

    
}