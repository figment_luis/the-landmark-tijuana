<?php namespace App\Models;

use CodeIgniter\Model;

class EventsModel extends Model
{
    protected $table      = 'events';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['title'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    function get_recents_events(){  //recent, si es 1 es reciente, si es 0, es próximo
        $db      = \Config\Database::connect();
        $builder = $db->table('events');
        $builder->select('*');
        $builder->where('recent', 1);
        $builder->where('deleted_at', 0);
        $builder->orderBy('id', 'DESC');
        $builder->limit(4);
        $query = $builder->get()->getResultArray();
        return $query;
    }

    function get_upcoming_events(){
        $db      = \Config\Database::connect();
        $builder = $db->table('events');
        $builder->select('*');
        $builder->where('recent', 0);
        $builder->where('deleted_at', 0);
        $builder->orderBy('id', 'DESC');
        $builder->limit(4);
        $query = $builder->get()->getResultArray();
        return $query;
    }
}
