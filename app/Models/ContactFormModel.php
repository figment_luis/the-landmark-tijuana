<?php

namespace App\Models;

use CodeIgniter\Model;

class ContactFormModel extends Model
{
    protected $table                = 'contacts';
    protected $primaryKey           = 'id';
    protected $insertID             = 1;
    protected $returnType           = 'object';
    protected $useSoftDeletes       = true;
    protected $protectFields        = true;
    protected $allowedFields        = ['name', 'email', 'telephone', 'information_type'];

    protected $useTimestamps        = true;
    protected $dateFormat           = 'datetime';
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

}