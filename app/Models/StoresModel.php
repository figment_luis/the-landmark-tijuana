<?php namespace App\Models;

use CodeIgniter\Model;

class StoresModel extends Model
{
 
    public function getAllStores()
    {
        $query = $this->query('SELECT * FROM stores');
        return $query->getResultArray();
    }

    public function getStoresByCategory($cat = false)
    { 
        if(!$cat)
            return false;
        if ($cat == 1)
            $query = $this->query("SELECT s.* ,  sub.subcat_name, sub.subcat_slug FROM categories_has_stores as cs  left join stores s on s.id=cs.stores_id  left join subcategories_has_stores sch on s.id=sch.stores_id  left join subcategories sub on sch.subcategory_id=sub.id  where s.deleted_at = 0 and cs.category_id=$cat or cs.category_id=2  order by s.name");
        else
            $query = $this->query("SELECT s.* ,  sub.subcat_name, sub.subcat_slug FROM categories_has_stores as cs  left join stores s on s.id=cs.stores_id  left join subcategories_has_stores sch on s.id=sch.stores_id  left join subcategories sub on sch.subcategory_id=sub.id  where s.deleted_at = 0 and cs.category_id=$cat order by s.name");
        return $query->getResultArray();
    }
    
    public function getStoreBySlug($slug = false)
    {
        if(!$slug)
            return false;

        $query = $this->query("SELECT * FROM stores WHERE deleted_at = 0 AND slug = '$slug' ");
        return $query->getResultArray();
    }
}