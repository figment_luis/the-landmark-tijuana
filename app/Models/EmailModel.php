<?php

namespace App\Models;

use CodeIgniter\Model;

class EmailModel extends Model
{
    private $email = null;

    public function __construct()
    {
        $this->email = \Config\Services::email();
    }

    public function send($from, $to, $email, $name, $subject, $data, $view)
    {
        $this->prepare();

        $this->email->setFrom($from, $name);
        $this->email->setTo($to);
        $this->email->setReplyTo($email, $name);
        $this->email->setSubject($this->getSubject($subject, $name));
        $this->email->setMessage(view('emails/' . $view, $data));
        $emailSended = $this->email->send();
        return $emailSended;
        // dd($this->email->printDebugger());
    }

    private function prepare()
    {
        // Protocol
        $config['protocol'] = 'smtp';
        // Host
        $config['SMTPHost'] = 'smtp.mailtrap.io';
        // Port
        $config['SMTPPort'] = 2525;
        // User
        $config['SMTPUser'] = '7033bc6fb0e59c';
        // Pass
        $config['SMTPPass'] = 'e32a2476319d67';
        // Support HTML
        $config['mailType'] = 'html';

        $this->email->initialize($config);
    }

    private function getSubject($type, $name)
    {
        switch ($type) {
            case 'comercial':
                return "[".strtoupper($type)."] - ".ucwords(mb_strtolower($name));
            case 'residencial':
                return "[".strtoupper($type)."] - ".ucwords(mb_strtolower($name));
            case 'corporativo':
                return "[".strtoupper($type)."] - ".ucwords(mb_strtolower($name));
        }
    }
}
