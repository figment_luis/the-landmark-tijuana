<?php namespace App\Models;

use CodeIgniter\Model;

class NewsletterModel extends Model
{

    protected $table      = 'newsletter';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'modified';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [
        'email' => 'required|valid_email|is_unique[newsletter.email]'
    ];

    protected $validationMessages = [
        'email' =>[
            'is_unique'=>'Este correo ya se encuentra registrado'
        ]
    ];
    protected $skipValidation     = false;
 
}