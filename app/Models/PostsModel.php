<?php namespace App\Models;

use CodeIgniter\Model;

class PostsModel extends Model
{
    protected $table      = 'posts';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['title'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    function get_last_posts($qty=9){ 
        $db      = \Config\Database::connect();
        $builder = $db->table('posts');
        $builder->select('*');
        $builder->where('deleted_at =', 0);
        $builder->orderBy('id', 'DESC');
        $builder->limit($qty);
        $query = $builder->get()->getResultArray();
        return $query;
    }
}
