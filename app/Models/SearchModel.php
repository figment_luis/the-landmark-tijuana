<?php namespace App\Models;

use CodeIgniter\Model;



class SearchModel extends Model
{

    protected $table      = 'stores';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['title'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
 
    public function getSearchResults($search = '')
    { 
        $query = $this->query("SELECT c.name as category, s.*  FROM stores s LEFT JOIN categories_has_stores as cs on cs.stores_id = s.id LEFT JOIN categories c ON c.id = cs.category_id where s.deleted_at = 0 and (s.name like '%$search%' or s.keywords like '%$search%') order by s.name");


        return $query->getResultArray();
    }
    
}