<?php namespace App\Controllers;

use App\Models\PostsModel;
use App\Models\StoresModel;
use App\Models\SearchModel;
use App\Models\EventsModel;
use App\Models\NewsletterModel;
use App\Models\SubcategoriesModel;
use App\Helpers\Promotion;
use App\Models\PopupModel;

class Main extends BaseController
{
	public function index()
	{
		$postsModel = new PostsModel($db);
		$data['news'] = $postsModel->get_last_posts();

		$eventsModel = new EventsModel($db);
		$subcategoriesModel = new SubcategoriesModel($db);
		$promotion = new Promotion;
        $popupModel = model('PopupModel');

		$uri = service('uri');

		$data['uri']=$uri;
		$data['promotion'] = $promotion;
		$data['subCategories'] = $subcategoriesModel->get_subcategories_data();
		$data['eventsRecents'] = $eventsModel->get_recents_events();
		$data['eventsUpcoming'] = $eventsModel->get_upcoming_events();
        $data['popup'] = $popupModel->where('enabled', 1)->first();

		return view('main', $data);
	}


	public function categories($page = NULL, $slug = NULL)
	{

		$uri = service('uri');


		$autorized = ['shopping','','gastronomia','entretenimiento','servicios', 'filtro'];
		if(!in_array($page, $autorized))
		{
			throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
		}

		$postsModel = new PostsModel($db);
		$StoresModel = new StoresModel($db);
		$subcategoriesModel = new SubcategoriesModel($db);


		if($page == 'filtro'){

			$data['filter']=$slug;
			$data['uri']=$uri;
			$data['subCategories'] = $subcategoriesModel->get_subcategories_data();
			$data['title'] = $uri->getSegment(1);
			$data['news'] = $postsModel->get_last_posts(3);
			$cat_id = array_search($uri->getSegment(1), $autorized);  //Segment 1 = Category
			$data['stores'] = $StoresModel->getStoresByCategory($cat_id+1);

			return view('categories', $data);
		}

		else if($slug){
			$data['store'] =  $StoresModel->getStoreBySlug($slug);
			$data['page'] = $page;
			$data['title'] = $uri->getSegment(1);
			echo view('store-individual', $data);

		}
		else{

			$data['uri']=$uri;
			$data['subCategories'] = $subcategoriesModel->get_subcategories_data();
			$data['title'] = $page;
			$data['news'] = $postsModel->get_last_posts(3);
			// $data['stores'] = $StoresModel->getAllStores();
			$cat_id = array_search($page, $autorized);
			$data['stores'] = $StoresModel->getStoresByCategory($cat_id+1);


			return view('categories', $data);
		}
		// return view('stores', $data);
	}

	public function avisoprivacidad()
	{
		return view('privacy');
	}


	public function search()
	{
		$output = '';
		$query = '';
		$SearchModel = new SearchModel($db);

		helper('text');

		if ($this->request->isAJAX()) {
			$query = service('request')->getPost('query');
		}


		$data = $SearchModel->getSearchResults($query);

		if (empty($data)){
			$output .= '';
		}
		else{
			foreach($data as $item_result){
				if ($item_result['category'] == "Estilo de vida") {
					$item_result['category'] = "shopping";
				}
				$output .= '<div class="result"><a href="'.base_url(strtolower(esc(convert_accented_characters($item_result['category']))).'/'.$item_result['slug']).'" class="link-search">
				<h2>'.$item_result['name'].'</h2>
				<p>'.$item_result['description'].'</p>
				</a></div>';
			}
		}
		echo $output;
	}

	public function saveOnNewsletter(){

		$newsletterModel = new NewsletterModel($db);
		$request = \Config\Services::request();
		$data = ['email' => (string)$request->getPost('email')];

		$newsletterModel->save($data);

	}


	public function services()
	{

		$uri = service('uri');

		$postsModel = new PostsModel($db);

		$data['uri']=$uri;
		$data['news'] = $postsModel->get_last_posts(3);
		$data['title'] = $uri->getSegment(1);

		return view('services', $data);
		}
		// return view('stores', $data);

}
