<?php namespace App\Controllers;

use App\Models\CardModel;
use App\Models\ContactFormModel;
use App\Models\EmailModel;
use App\Models\PopupModel;
use App\Models\SlideModel;
use CodeIgniter\Database\Exceptions\DatabaseException;

class Page extends BaseController
{
	public function index()
	{
        $uri = service('uri');
        $popupModel = new PopupModel;

        $data['uri']=$uri;
        $data['popup'] = $popupModel->where('enabled', 1)->first();

		return view('pages/home', $data);
	}

    public function oficinas()
    {
        return view('pages/oficinas');
    }

    public function retail()
    {
        return view('pages/retail');
    }

    public function departamentos()
    {
        $slideModel = new SlideModel();
        $cardModel = new CardModel();

        $slides = $slideModel->findAll();
        $plans = $cardModel->where('type', 1)->get()->getResult();
        $experiences = $cardModel->where('type', 2)->get()->getResult();

        return view('pages/departamentos', compact('slides', 'plans', 'experiences'));
    }

    public function contacto()
    {
        return view('pages/contacto');
    }

    public function registro()
    {
        try {
            $rules = [
                'name' => [
                    'label' => 'nombre',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                    ]
                ],
                'email' => [
                    'label' => 'correo electrónico',
                    'rules' => 'required|valid_email',
                    'errors' => [
                        'required'    => 'El {field} es un dato requerido',
                        'valid_email' => 'El {field} no parece ser un dato válido'
                    ]
                ],
                'telephone' => [
                    'label' => 'número telefónico',
                    'rules' => 'required|numeric|min_length[10]|max_length[10]',
                    'errors' => [
                        'required'   => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres',
                        'numeric'    => 'El {field} debe tener valores numéricos'
                    ]
                ],
                'information_type' => [
                    'label' => 'tipo de información',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                    ]
                ]
            ];
            $input = $this->validate($rules);
            $response = [];
            if (!$input) {
                $response["status"]  = "invalid";
                $response["code"]    = 406;
                $response["message"] = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['errors']  = $this->validator->getErrors();
                return json_encode($response);
            }

            $name        = trim($this->request->getPost('name'));
            $email       = trim($this->request->getPost('email'));
            $telephone   = trim($this->request->getPost('telephone'));
            $subject     = $this->request->getPost('information_type');

            $emailModel = new EmailModel();
            $emailSended = $emailModel->send('no-reply@thorurbana.com', 'no-reply@thorurbana.com', $email, $name, $subject, compact('name', 'email', 'subject', 'telephone'), 'contact');

            if ($emailSended) {
                $contactModel = new ContactFormModel();
                $contactModel->insert([
                    'name'             => $name,
                    'email'            => $email,
                    'telephone'        => $telephone,
                    'information_type' => $subject,
                ]);
                $response['status']  = 'success';
                $response['code']    = 200;
                $response['message'] = 'Sus datos fueron registrados satisfactoriamente en el sistema. ';
                return json_encode($response);
            } else {
                $response['status']  = 'error';
                $response['code']    = 500;
                $response['message'] = 'Se detectó un error al tratar de procesar su información, favor de intentar más tarde.';
                return json_encode($response);
            }
        } catch (DatabaseException $e) {
            $response['status']  = 'error';
            $response['code']    = 500;
            $response['message'] = $e->getMessage();
            return json_encode($response);
        } catch (\Exception $e) {
            $response['status']  = 'error';
            $response['code']    = 500;
            $response['message'] = $e->getMessage();
            return json_encode($response);
        }
    }

}
