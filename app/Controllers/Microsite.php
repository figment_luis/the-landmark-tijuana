<?php
namespace App\Controllers;

use App\Helpers\Promotion;

class Microsite extends BaseController
{
    public function buenfin()
    {
        $promotion = new Promotion;
        $uri = service('uri');
        return view('microsite', compact('promotion', 'uri'));
    }
}
