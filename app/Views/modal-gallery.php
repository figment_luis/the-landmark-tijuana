<!-- Modal -->
<div class="modal fade" id="modal-gallery" tabindex="-1" role="dialog" aria-labelledby="Gallery"
aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="search-close container pt-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">< Regresar</span>
                </button>
            </div>
            <div class="modal-body search-content w-100">
                <div class="container text-center">
                    
                </div>
            </div>
        </div> 
    </div>
</div>