<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title><?= $promotion->micrositio_name;?></title>
    <link rel="icon" href="<?= base_url('assets/images/favicon.png'); ?>">
    <!-- Estilos CSS Microsite -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/microsite.css'); ?>">

    <!-- Script necesarios para la verificación de promociones activas  -->
    <script src="<?= base_url('assets/libs/moment/moment-with-locales.js');  ?>"></script>
    <script src="<?= base_url('assets/libs/moment/moment-timezone.min.js');  ?>"></script>
    <script src="<?= base_url('assets/libs/moment/moment-timezone-with-data.min.js');  ?>"></script>
    <script type="text/javascript">
      const current_time = moment().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");
      const date_open = '<?php echo $promotion->promo_start ?>';
      const date_close = '<?php echo $promotion->promo_end ?>';
      // Redireccionar si la promoción ya no se encuentra activa
      if (current_time <= date_open || current_time >= date_close){
          let url = '<?php echo base_url(); ?>'
          window.location.replace(url);
      }
      // Caso contrario se muestra el contenido de la promoción
    </script>

  </head>
  <body>
    <div class="microsite">
      <div class=" microsite__container">
        <img src="<?= base_url($promotion->micrositio_landing)."?v=".rand(); ?>"
             alt="<?= $promotion->micrositio_name; ?>"
             class="microsite__image">
      </div>
    </div>
    <!-- ========= SCRIPTS  ========= -->
    <script  src="<?= base_url('assets/libs/jquery/jquery-3.5.1.min.js') ?>"></script>
    <script>
      $(document).bind("contextmenu",function(t){return t.stopPropagation(),t.preventDefault(),t.stopImmediatePropagation(),!1});
    </script>
    <?php if($promotion->micrositio_tracking != ""): ?>

    <?php endif; ?>
  </body>
</html>
