<?php $uri = $uri = service('uri', current_url(true)); ?>

<div class="item-menu d-block mt-5 d-md-none"></div>
<div class="item-menu"><a href="<?= base_url('departamentos') ?>" title="Live" class="<?=($uri->getSegment(1)==='departamentos')?'active':''?>">DEPARTAMENTOS</a></div>
<div class="align-self-center d-none d-md-block px-2 divider">|</div>
<div class="item-menu"><a href="<?= base_url('retail') ?>" title="Shopping and Work" class="<?=($uri->getSegment(1)==='retail')?'active':''?>">RETAIL</a></div>
<div class="align-self-center d-none d-md-block px-2 divider">|</div>
<div class="item-menu"><a href="<?= base_url('oficinas') ?>" title="Shopping and Work" class="<?=($uri->getSegment(1)==='oficinas')?'active':''?>">OFICINAS</a></div>
<div class="align-self-center d-none d-md-block px-2 divider">|</div>
<div class="item-menu"><a href="<?= base_url('contacto') ?>" title="Contacto" class="<?=($uri->getSegment(1)==='contacto')?'active':''?>">CONTACTO</a></div>

