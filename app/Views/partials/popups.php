<?php if ($popup != null && is_object($popup)): ?>
    <div id="popup_desktop" class="popup" style="display: none;">
      <?php if($popup->link): ?>
        <a href="<?php echo $popup->link; ?>" class="popup__link" target="<?php echo $popup->target ?>" title="Popup The Landmark Tijuna">
          <img src="<?php echo base_url($popup->image_desktop)."?v=".rand(); ?>" class="popup__image" alt="Popup Desktop" title="Popup Desktop" />
        </a>
      <?php else: ?>
        <img src="<?php echo base_url($popup->image_desktop)."?v=".rand(); ?>" class="popup__image" alt="Popup Desktop" title="Popup Desktop"/>
      <?php endif; ?>
    </div>

    <div id="popup_mobile" class="popup--mobile" style="display: none;">
      <?php if($popup->link): ?>
        <a href="<?php echo $popup->link; ?>" class="popup__link--mobile" target="<?php echo $popup->target; ?>" title="Popup The Landmark Tijuna">
          <img src="<?php echo base_url($popup->image_mobile)."?v=".rand(); ?>" class="popup__image--mobile" alt="Popup Mobile" title="Popup Mobile"/>
        </a>
      <?php else: ?>
        <img src="<?php echo base_url($popup->image_mobile)."?v=".rand(); ?>" class="popup__image--mobile" alt="Popup Mobile" title="Popup Mobile"/>
      <?php endif; ?>
    </div>
<?php endif; ?>