<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Información de Contacto</title>
</head>
<body style="background-color: #272727; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; color: #E3E3E3;">
    <div style="margin: 32px auto 0; max-width: 600px;">
        <img src="<?= base_url('assets/images/landmark.png')?>" alt="Logotipo Thorurbana" style="display: block; margin: 0 auto 48px; max-width: 100%;">
        <p style="line-height: 1.4; margin-bottom: 16px;">Estimad@ <strong><?= $name ?></strong>, gracias por tu registro. En breve uno de nuestros asesores se pondrá en contacto contigo para dar un mejor seguimiento a tu solicitud.</p>
        <ul style="margin: 32px 0;">
            <li style="margin-bottom: 6px;">Email: <span style="color: #fc498a;"><?= $email ?></span></li>
            <li style="margin-bottom: 6px:">Teléfono: <span style="color: #fc498a;"><?= $telephone ?></span></li>
            <li style="margin-bottom: 6px;">Tipo de Información: <span style="color: #fc498a;"><?= $subject ?></span></li>
        </ul>
        <p style="line-height: 1.4; margin-bottom: 16px;">Atentamente, el equipo de The Landmark Tijuana.</p>
    </div>
</body>
</html>