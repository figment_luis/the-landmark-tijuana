<?php $uri = $uri = service('uri', current_url(true)); ?>
<div class="bar-footer"></div>
<footer id="mapa" class="pt-5 block-footer">
	<div class="container">
        <?php if ($uri->getSegment(1) !== 'contacto'): ?>
            <div class="row">
                <div class="col-12">
                    <h4>CONTACTO</h4>
                </div>
                <div class="col-md-6 col-sm-12 mt-3">
                    <a href="https://www.google.com.mx/maps/place/The+Landmark+Tijuana/@32.518753,-117.0202635,17z/data=!3m1!4b1!4m5!3m4!1s0x80d949cd98c66ccf:0xdddfe9acee4d7744!8m2!3d32.518753!4d-117.0180748?shorturl=1"
                       target="_blank" title="Ubicación The Landmark Tijuana" rel="noopener" rel="noreferrer">
                        <img class="img-fluid  w-100" src="<?= base_url('assets/images/maps.png'); ?>"
                             alt="The Landmark Tijuana - Google Maps" title="Google Maps The Landmark Tijuana" width="660" height="485" loading="lazy">
                    </a>
                </div>
                <div class="col-md-6 col-sm-12 align-self-center">
                    <div class="my-auto pl-5 pt-5 pt-md-0">
                        <div class="mb-4 pl-2">
                            <span class="icon-dir"><img src="<?= base_url('assets/images/icons/icon-location.svg'); ?>" alt="Dirección" title="Dirección: The Landmark Tijuana" loading="lazy"></span>
                            <p><b>DIRECCIÓN</b></p>
                            <p>Blvd. Agua Caliente 10223, aviación <br>Tijuana, BC, C.P. 22014</p>
                        </div>
                        <div class="mb-4 pl-2">
                            <span class="icon-dir"><img src="<?= base_url('assets/images/icons/icon-phone.svg'); ?>" alt="Teléfono" title="Teléfono: The Landmark Tijuana" loading="lazy"></span>
                            <p><b>TELÉFONOS</b></p>
                            <p class="info"><a href="tel:+526449068442" title="6449068442">(644) 906 8442</a></p>
                        </div>
                        <div class="mb-4 pl-2">
                            <span class="icon-dir"><img src="<?= base_url('assets/images/icons/mail.svg'); ?>" alt="Email" title="Email: The Landmark Tijuana" style="width: 25px;" loading="lazy"></span>
                            <p><b>CORREO ELECTRÓNICO</b></p>
                            <p class="info"><a href="mailto:contacto@landmarktijuana.com" title="contacto@landmarktijuana.com" style="word-wrap: break-word;">contacto@landmarktijuana.com</a></p>
                            <p class="info"><a href="mailto:leasing@landmarktijuana.com" title="leasing@landmarktijuana.com" style="word-wrap: break-word;">leasing@landmarktijuana.com</a></p>
                        </div>
                        <div class="social-landmark mb-4 pl-2">
                            <p><b>SÍGUENOS EN NUESTRAS REDES SOCIALES</b></p>
                            <a href="https://www.facebook.com/TheLandmarkTijuana" rel="noopener" rel="noreferrer" class="mr-3" target="_blank" title="Facebook The Landmark Tijuana"><i class="pt-4 fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/thelandmarktijuana/" rel="noopener" rel="noreferrer" class="mr-3" target="_blank" title="Instagram The Landmark Tijuana"><i class="pt-4 fa fa-instagram fa-2x" aria-hidden="true"></i></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="w-100 mt-5">
	    <?php endif; ?>
        <div class="mb-3 row">
            <div class="col-md-3 col-sm-12">
                <img src="<?= base_url('assets/images/thor-urbana-black.svg') ?>" alt="Logotipo Thor Urbana"
                     title="Logotipo Thor Urbana" class="img-fluid" width="302" height="70" loading="lazy">
            </div>
            <div class="social-thor col-md-9 col-sm-12">
                <a href="https://www.facebook.com/thorurbana" rel="noopener" rel="noreferrer" class="mx-3" target="_blank" title="Facebook Thor Urbana"><i class="pt-4 fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/thorurbana" rel="noopener" rel="noreferrer" class="mx-3" target="_blank" title="Instagram Thor Urbana"><i class="pt-4 fa fa-instagram fa-2x" aria-hidden="true"></i></i></a>
                <a href="https://twitter.com/ThorUrbana" rel="noopener" rel="noreferrer" class="mx-3" target="_blank" title="Twitter Thor Urbana"><i class="pt-4 fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="pt-3 pb-3 privacy-text">
			© <?= date('Y') ?> The LANDMARK TIJUANA <a target="_blank" href="<?= base_url('assets/files/aviso-privacidad.pdf'); ?>" class="text-underline" title="Aviso de privacidad clientes Thor Urbana"> <u>AVISO DE PRIVACIDAD</u></a>
		</div>
	</div>
</footer>