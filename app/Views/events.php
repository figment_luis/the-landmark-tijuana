<section class="title-section text-center">
	<div class="d-flex align-items-center justify-content-center">
		<h2>Eventos</h2>
		<span>EVENTOS</span>
	</div>
	<br>
	<p class="mt-3">Disfruta de los mejores eventos y experiencias únicas durante todo el año</p>
	<img class="d-block mx-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
</section>

<section  id="events" class="py-5">
        <div class="container">                
 
            <div class="selector-rp d-flex justify-content-lg-center align-items-center">
                <h4 class="selector sel-r event-active" onClick="recentsChange('assets/images/events/<?php echo $eventsRecents[0]['id']; ?>/<?php echo $eventsRecents[0]['slug'];?> 1.jpg');">RECIENTES</h4>
                <?php /*<div class="align-self-center d-none d-lg-block px-2 divider">|</div>
                <h4 class="selector sel-p" onClick="upcomingsChange('assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug'];?> 1.jpg');">PRÓXIMOS</h4>
                */ ?>
            </div>

            <div class="row recent-events fade-in">
                <div class="col-lg-5 event-selector">
                    <?php if(count($eventsRecents)): ?>
                        <?php foreach ($eventsRecents as $index => $event): ?>
                            <?php if( $index > 3) break; ?>
                            <div class="event <?php echo $index === 0 ? 'event-active default' : '' ?>" id="reciente1"
                                onClick="switchEventImage('assets/images/events/<?= $event['id'];?>/<?= $event['slug'];?> 1.jpg',<?= $event['num_img'];?>);">
                                <h4><?= $event['name']; ?></h4>
                                <p class="event-description <?php echo $index === 0 ? 'd-block' : 'd-none' ?>"><?= $event['event_description']; ?></span></p>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="image-container col-lg-7">
                    <div class="event-image"><img src="assets/images/events/<?= $eventsRecents[0]['id'];?>/<?= $eventsRecents[0]['slug'];?> 1.jpg" alt=""></div>
                    <a href="assets/images/events/<?= $eventsRecents[0]['id']; ?>/<?= $eventsRecents[0]['slug'];?> 1.jpg" class="galery-btn" data-lightbox="galery-recents">Ver más fotos</a>
                    <?php for ($i=2; $i<=$eventsRecents[0]['num_img']; $i++): ?>
                        <a href="assets/images/events/<?= $eventsRecents[0]['id'] . "/" . $eventsRecents[0]['slug'] . " " . $i . ".jpg" ?>" data-lightbox="galery-recents"></a>
                    <?php endfor; ?>
                    
                </div>
            </div>

            <?php /*<div class="row upcoming-events d-none fade-in">
                <div class="col-lg-5 event-selector">
                    <div class="event event-active default" id="reciente1"
                    onClick="switchEventImage('assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug']; ?> 1.jpg');">
                        <h4><?php echo $eventsUpcoming[0]['name']; ?></h4>
                        <p class="event-description d-block"><?php echo $eventsUpcoming[0]['event_description']; ?></span></p>
                    </div>
                    <div class="event" id="reciente1" onClick="switchEventImage('assets/images/events/<?php echo $eventsUpcoming[1]['id']; ?>/<?php echo $eventsUpcoming[1]['slug']; ?> 1.jpg');">
                        <h4><?php echo $eventsUpcoming[1]['name']; ?></h4>
                        <p class="event-description d-none"><?php echo $eventsUpcoming[1]['event_description']; ?></span></p>
                    </div>
                    <div class="event" id="reciente1" onClick="switchEventImage('assets/images/events/<?php echo $eventsUpcoming[2]['id']; ?>/<?php echo $eventsUpcoming[2]['slug']; ?> 1.jpg');">
                        <h4><?php echo $eventsUpcoming[2]['name']; ?></h4>
                        <p class="event-description d-none"><?php echo $eventsUpcoming[2]['event_description']; ?></span></p>

                    </div>
                    <div class="event" id="reciente1" onClick="switchEventImage('assets/images/events/<?php echo $eventsRecents[3]['id']; ?>/<?php echo $eventsRecents[3]['slug']; ?> 1.jpg');">
                        <p class="event-description d-none"><?php echo $eventsUpcoming[3]['event_description']; ?></span></p>
                        <h4><?php echo $eventsUpcoming[3]['name']; ?></h4>
                    </div>
                </div>
                <div class="image-container col-lg-7">
                <div class="event-image"><img src="assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug']; ?> 1.jpg" alt=""></div>
                    <a href="assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug']; ?> 1.jpg" class="galery-btn" data-lightbox="galery-upcoming">Ver más fotos</a>
                    <a href="assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug']; ?> 2.jpg" data-lightbox="galery-upcoming"></a>
                    <a href="assets/images/events/<?php echo $eventsUpcoming[0]['id']; ?>/<?php echo $eventsUpcoming[0]['slug']; ?> 3.jpg" data-lightbox="galery-upcoming"></a>
                </div>
            </div> */?>
        </div>
</section>