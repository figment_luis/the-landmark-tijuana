<?php
    if ((base_url()."/") == current_url()):?>
    <video autoplay muted playsinline loop id="mainVideo">
        <source src="<?= base_url('assets/videos/LandmarkTijuana.mp4'); ?>" type="video/mp4">
    </video>
<?php endif; ?>

<div>
    <div class="top-header">
        <h1 style="display: none">The Landmark Tijuana</h1>
        <h2 style="display: none">Lifestyle Retail Center</h2>
        <div class="text-center py-5 main-logo">
            <a href="<?= base_url(); ?>" title="The Landmark Tijuana">
                <img src="<?= base_url('assets/images/landmark.png'); ?>" alt="Logotipo The Landmark Tijuana"
                     title="Logotipo The Landmark Tijuana" width="258" height="158" loading="lazy">
            </a> 
        </div>
        <div class="menu d-flex justify-content-center  flex-column flex-md-row text-light text-center">
            <?=  $this->include('options-menu') ?>
        </div>
    </div>
    <div class="bottom-header">
        <div class="search-button">
            <a class="scroll-down"><img src="<?= base_url('assets/images/arrow-down.png') ?>" alt="ScrollDown" title="Scroll Down" width="24" height="14"></a>
        </div>
    </div>
</div>