<section id="newsletter" class="mt-5">
    <div class="container">
        <hr>
        <form id="newsletter-form">
            <?= csrf_field() ?>
            <div class="d-flex justify-content-center align-items-center align-content-between flex-column flex-md-row my-5">
                <p class="my-auto pr-3"><span class="text-signature">Recibe</span> <b>las mejores noticias</b></p>
                <input id="email" type="email" name="email" class="input-suscribe" placeholder="E-MAIL" required>
                <button type="submit" class="btn btn-mark btn-suscribe ml-md-2">SUSCRÍBETE</button>
            </div>
        </form>
    </div>
</section>	 