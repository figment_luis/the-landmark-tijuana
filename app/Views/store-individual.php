<?= $this->extend('layout') ?>


<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?><?= ucfirst($store[0]['name']); ?><?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?php if(strtoupper($title) == "GASTRONOMIA"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/gastronomia.jpg")?><?= $this->endSection() ?>
<?php elseif(strtoupper($title) == "SHOPPING"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/shopping.jpg")?><?= $this->endSection() ?>
<?php elseif(strtoupper($title) == "ENTRETENIMIENTO"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/landmark_cinemex.jpg")?><?= $this->endSection() ?>
<?php elseif(strtoupper($title) == "SERVICIOS"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/servicios.jpg")?><?= $this->endSection() ?>
<?php else: ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/header-side.jpg")?><?= $this->endSection() ?>
<?php endif; ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?> 
	<main class="store-principal-section position-relative" id="info-page">
        <div class="subtitle-info w-100 text-uppercase"><?= esc($store[0]['name']); ?></div>
        <div class="container">
            <div class="d-flex justify-content-center return-button mb-2">
                <div class="p-2"><a href="<?= base_url(esc($page)); ?>">
                        < Regresar</a>
                </div>
            </div>
            <div class="row grid-boutique d-md-flex justify-content-between">
                <div class="col-sm-12 col-md-6 mb-4 store-img text-center">
					<div class="cycle-slideshow" data-cycle-swipe=true data-cycle-swipe-fx=scrollHorz data-cycle-fx="scrollHorz"   data-cycle-timeout="4000" data-cycle-speed=900  data-cycle-slides="> img"  data-cycle-pager=".pagers_slide" data-cycle-prev="#prev" data-cycle-next="#next">
						<?php 
							$url = 'assets/images/stores/'.$store[0]['id'].'/';
							$url_alternative = 'assets/images/stores/0/';
							if(is_dir($url)){
								$folder = opendir($url);	
							}
							else{
								$folder = opendir($url_alternative);
								$value['id'] = 0;
							}		
							$pic_types = array("jpg", "jpeg", "png");
							$images = array();
							while($file = readdir($folder))
							{
								if(in_array(substr(strtolower($file),strrpos($file,".") + 1),$pic_types))
								{
									array_push($images,$file);
								}
							}
                            closedir($folder);
                            sort($images);

							for($i = 0;$i < count($images);$i++)
							{
								echo '<img class="img-fluid w-100" src="'.base_url('assets/images/stores/'.$store[0]['id'].'/'.$images[$i]).'" alt="'.$store[0]['name'].'">';
							}
						?>
						</div>
                    	<!-- <img src="<?= base_url('assets/images/store/ck.jpg'); ?>" alt="imagen-presentacion"> -->
                </div>
                <div class="col-sm-12 col-md-6">
                                       
                    <div class="store-content">
                        <?php if($store[0]['id']==8):?>
                            <h1 class="store-title"><?= esc($store[0]['name']); ?></h1>
                        <?php else:?>
                        <h1 class="store-title text-uppercase"><?= esc($store[0]['name']); ?></h1>
                        <?php endif?>


                        <?php if($store[0]['name']=='Concierge'):?>
                            <p class="mb-3">En nuestro espacio de concierge, usted podrá encontrar servicios como:</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li class="">Préstamo de sillas de ruedas</li>
                                        <li class="">Préstamo de paraguas</li>
                                        <li class="">Guarda compras</li>
                                        <li class="">Asistente de compras personal</li>
                                        <li class="">Reservaciones y sugerencias de restaurantes especializados</li>
                                        <li class="">Periodicos</li>
                                        <li class="">Informacion sobre eventos especiales</li>
                                        <li class="">Paramédico</li>
                                        <li class="">Internet  Wi-Fi</li>
                                        <li class="">Servicios Turísticos</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li class="">Información sobre Hoteles</li>
                                        <li class="">Renta de automóviles</li>
                                        <li class="">Información sobre rutas de evacuación y salidas de emergencia</li>
                                        <li class="">Recepción de comentarios y sugerencias</li>
                                        <li class="">Asistencia y orientación sobre cualquier pregunta realizada</li>
                                        <li class="">Bolero</li>
                                        <li class="">Botellas de agua de cortesía</li>
                                        <li class="">Bolsas para regalo</li>
                                        <li class="">Juegos de mesa</li>
                                    </ul>
                                </div>
                            </div>
                           
                        <?php else:?>
                            <p class="mb-2"><?= esc($store[0]['description']); ?></p>
                        <?php endif ?>


                        <div class="row store-info">
                            <div class="col-md-5">
                                <h3>Te esperamos en: <br>Nivel <?= strtoupper( esc($store[0]['map_location']) ); ?></h3>
                                <?php if( !esc($store[0]['telephone']) == '' ): ?>
                                    <h3>TELÉFONO</h3>
                                    <p><a href="tel:<?= strtoupper( esc($store[0]['telephone']) ); ?>"><?= strtoupper( esc($store[0]['telephone']) ); ?></a></p>
                                    <?= isset($store[0]['telephone_2'])?'<p><a href="tel:'.strtoupper( esc($store[0]['telephone_2']) ).'">'.strtoupper( esc($store[0]['telephone_2']) ).'</a></p>':''; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-7">
                                <h3>HORARIOS</h3>
                                <?php 
                                    $hourhands = esc($store[0]['hourhand']);
                                    $hourhand = explode(',',$hourhands);
                                    foreach($hourhand as $hour)  {
                                        echo ("<p>".$hour."</p>");                                    }
                                ?>
                            </div>

                            <!--Show Thor2Go logo, only if the store has the service-->
                            <?php if($store[0]["thor2go"]==1):?>
                                <div class="col-md-12 d-flex align-content-center justify-content-start flex-column flex-md-row">
                                    <h3 class="d-inline align-self-start align-self-md-center">DISPONIBLE EN:</h3>
                                    <a class="thor2go-image" 
                                        data-toggle="popover" 
                                        title="THOR2GO" 
                                        data-content="Para recibir tus compras hasta la puerta de tu casa con Thor2Go, es necesario ponerte en contacto directamente con la tienda.">
                                        <img src="<?= base_url('assets/images/Thor2go.png'); ?>" alt="Thor2Go">
                                    </a>
                                </div>
                            <?php endif; ?>

                            <!--Show the QR Code, only if the store has one-->
                            <?php if($store[0]["link_qr"]==1):?>
                                <div class="store_qr col-md-12">
                                    <h3 class="">CONSULTA NUESTRO MENÚ</h3>
                                    <img src="<?= base_url('assets/images/qr_codes/'.esc($store[0]['slug']).'.png'); ?>" width="185px" alt="QR CODE">
                                </div>
                            <?php endif; ?>


                        </div>        
                    </div> <!--contenido-boutique-->
                </div> <!--fin de segunda columna-->


            </div>
            <!--Grid Boutique-->
        </div>
        <!--Seccion Principal-->
	</main>

<?= $this->endSection() ?>

<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>

<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
<link rel="stylesheet" href="<?= base_url('assets/css/store.css'); ?>">
<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
<script src="<?= base_url('assets/js/jquery.cycle2.js');  ?>"></script>
<script src="<?= base_url('assets/js/jquery.cycle2.swipe.js');  ?>"></script>
<?= $this->endSection() ?>