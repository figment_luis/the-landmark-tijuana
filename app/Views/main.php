<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Visita<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>main-header<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?= $this->section('image-header') ?><?= $this->endSection() ?>

<!-- ========= Popups Dinámicos ========= -->
<?= $this->section('popups') ?>
    <?= $this->include('partials/popups') ?>
<?= $this->endSection()?>


<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
	<section class="mycard" id="info-page">
		<div class="container">
            <div class="mycard-watermark">TIJUANA</div>
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 mycard-header">
					<img class="img-fluid d-none d-sm-block mycard-image" src="<?= base_url('assets/images/info-home.png'); ?>" alt="The Landmark Tijuana">
					<div class="mycard-brand">LANDMARK<br>TIJUANA</div>
				</div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div class="mycard-description">
                        <div class="mycard-title">Somos</div>
                        <p>Un desarrollo de usos mixtos donde podrás vivir, trabajar y disfrutar de tu vida, en un entorno diseñado para estimular todas tus experiencias de una forma nunca antes vista.</p>
                        <p>The Landmark Tijuana está conformado por una torre residencial, un centro comercial, una torre de oficinas y un hotel, todo este desarrollo diseñado bajo el concepto Lifestyle Retail Center, una idea única que consiste en conectar a sus habitantes en los diferentes espacios para una mayor interacción y comodidad.</p>
                        <p>Este proyecto de usos múltiples desarrollado en conjunto por Thor Urbana y GFA, cuenta con más de 30,000 m² de áreas comerciales, 14,000 m² de oficinas modernas AAA, 193 condominios y un hotel con más de 200 habitaciones.</p>
                    </div>
                </div>
			</div>
		</div>
	</section>
<?= $this->endSection() ?>



<!-- ========= CONTENT EXTRA  ========= -->

<?= $this->section('content-extra') ?>
	<?= $this->include('partials/info-location') ?>
    <?= $this->include('partials/info-thorurbana') ?>
    <?= $this->include('partials/info-architects') ?>
<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
	<link rel="stylesheet" href="<?= base_url('assets/css/promotion.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/popup.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/libs/fancybox/jquery.fancybox.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/events.css'); ?>">
<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
	<script src="<?= base_url('assets/js/events.js');  ?>"></script>
	<script src="<?= base_url('assets/libs/fancybox/jquery.fancybox.min.js');  ?>"></script>
	<script src="<?= base_url('assets/libs/moment/moment-with-locales.js');  ?>"></script>
	<script src="<?= base_url('assets/libs/moment/moment-timezone.min.js');  ?>"></script>
	<script src="<?= base_url('assets/libs/moment/moment-timezone-with-data.min.js');  ?>"></script>
    <!-- Lógica Popups Dinámicos -->
    <?php if ($popup != null && is_object($popup)): ?>
        <script>
            const current_time = moment().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");

            const date_open = '<?php echo $popup->start_date ?>';
            const date_close = '<?php echo $popup->end_date ?>';

            let popupvisible = false;
            let isMobile = false;

            // Verificar si el popup aun debe estar visible en el sitio
            if (current_time >= date_open && current_time <= date_close){
                popupvisible = true;
            }

            // Verificar si el sitio esta siendo consumido por algún dispositivo movil
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                isMobile = true;
            }

            // Mostrar el popup correspondiente [Desktop || Mobile]
            if(popupvisible){
                if(isMobile === false){
                    setTimeout(function(){
                               $.fancybox.open({
                                               src: '#popup_desktop'
                                               });
                               }, 1000);
                } else {
                    setTimeout(function(){
                               $.fancybox.open({
                                               src: '#popup_mobile'
                                               });
                               }, 1000);
                }
            }
        </script>
    <?php endif; ?>
<?= $this->endSection() ?>
