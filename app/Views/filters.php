
<section id="filtros" class="filterSubCategories container px-5 mb-5">
    <div class="d-md-flex d-lg-flex justify-content-center align-content-center mb-5 text-center">
        <h3 class="text-center mb-3 mr-sm-3 mr-lg-3">Encuentra tus tiendas favoritas</h3>
        <span id="btn-filter" class="btn-filter mb-3">Filtros <i class="fa fa-angle-down" aria-hidden="true"></i></span>
    </div>
    <div class="filter-content">
        <div class="row">
            <div class="col-md-4 col-sm-12 mt-4">
                <h2>MODA</h2>
                <ul>
                    <?php foreach ($subCategories as $subcategory):
                        if(esc($subcategory["name"]) == "Moda"):?>
                            <?php if ( esc($uri->getSegment(1)) == "shopping"):?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php else: ?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>" href="<?= base_url('shopping/filtro/'.$subcategory["subcat_slug"]); ?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 mt-4">
                <h2>ESTILO DE VIDA</h2>
                <ul>
                    <?php foreach ($subCategories as $subcategory):
                        if(esc($subcategory["name"]) == "Estilo de vida"):?>
                            <?php if ( esc($uri->getSegment(1)) == "shopping"):?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php else: ?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>" href="<?= base_url('shopping/filtro/'.$subcategory["subcat_slug"]); ?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 mt-4">
                <h2>ENTRETENIMIENTO</h2>
                <ul>
                    <?php foreach ($subCategories as $subcategory):
                        if(esc($subcategory["name"]) == "Entretenimiento"):?>
                            <?php if ( esc($uri->getSegment(1)) == "entretenimiento"):?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php else: ?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>" href="<?= base_url('entretenimiento/filtro/'.$subcategory["subcat_slug"]); ?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-12 mt-4">
                <h2>GASTRONOMÍA</h2>
                <ul>
                    <?php foreach ($subCategories as $subcategory):
                        if(esc($subcategory["name"]) == "Gastronomía"):?>
                            <?php if ( esc($uri->getSegment(1)) == "gastronomia"):?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php else: ?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>" href="<?= base_url('gastronomia/filtro/'.$subcategory["subcat_slug"]); ?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!--<div class="col-md-4 col-sm-12 mt-4">
                <h2>SERVICIOS</h2>
                <ul>
                    <?php foreach ($subCategories as $subcategory):
                        if(esc($subcategory["name"]) == "Servicios"):?>
                            <?php if ( esc($uri->getSegment(1)) == "servicios"):?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php else: ?>
                                <li><a id="<?php echo esc($subcategory["subcat_slug"]);?>" href="<?= base_url('servicios/filtro/'.$subcategory["subcat_slug"]); ?>"><?php echo esc($subcategory["subcat_name"])?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>-->
            <?php if ( esc($uri->getSegment(1)) != ""):?>
                <div class="col-md-4 col-sm-12 mt-4">
                    <h2>A - Z</h2>
                    <section class="letter-filter">
                            <span id="A_filter">A</span>
                            <span id="B_filter">B</span>
                            <span id="C_filter">C</span>
                            <span id="D_filter">D</span>
                            <span id="E_filter">E</span>
                            <span id="F_filter">F</span>
                            <span id="G_filter">G</span>
                            <span id="H_filter">H</span>
                            <span id="I_filter">I</span>
                            <span id="J_filter">J</span>
                            <span id="K_filter">K</span>
                            <span id="L_filter">L</span>
                            <span id="Ñ_filter">Ñ</span>
                            <span id="N_filter">N</span>
                            <span id="M_filter">M</span>
                            <span id="O_filter">O</span>
                            <span id="P_filter">P</span>
                            <span id="Q_filter">Q</span>
                            <span id="R_filter">R</span>
                            <span id="S_filter">S</span>
                            <span id="T_filter">T</span>
                            <span id="U_filter">U</span>
                            <span id="V_filter">V</span>
                            <span id="W_filter">W</span>
                            <span id="X_filter">X</span>
                            <span id="Y_filter">Y</span>
                            <span id="Z_filter">Z</span>
                    </section>
                </div>
            </div>
            <div class="text-center clean-filter mt-5">
                    <span id="filter-all" class="active btn-filter">Limpiar Filtros</span>
            </div>
        <?php endif; ?>
    </div>
</section>