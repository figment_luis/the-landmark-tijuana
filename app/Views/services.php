<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?><?= ucfirst($title); ?><?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/servicios.jpg")?><?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
	<section class="pt-5 pb-5 position-relative info-page" id="info-page">
		<div class="container position-relative">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<img class="img-fluid" src="<?= base_url('assets/images/about-us.jpg'); ?>" alt="The Landmark Guadalajara">
					<span class="text-info-deco"><?= strtoupper($title); ?></span>
				</div>
			</div>
			<div class="text-info-section col-sm-12">
				<span class="title-info">Diversas opciones</span>
				<p>The Landmark - ofrece con orgullo una variedad de servicios cinco estrellas para garantizar que cada visita sea agradable. Nuestro servicio de Concierge ofrece a todos nuestros visitantes lo mejor en servicio al cliente. Nuestros embajadores brindan asistencia y guían a los huéspedes a través de su experiencia de compra con las últimas ofertas, direcciones, reservas y una variedad de servicios. Asegúrese de visitar Concierge, ubicado en el nivel inferior para recibir asistencia y consultas. 
				<img class="d-block mt-3" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
				</p>		
			</div>
		</div>
		<div class="subtitle-info">
            <?= strtoupper($title); ?>
		</div>
	</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>

<!-- ========= Services  ========= -->

    <section id="services" class="my-5">
        <div class="container">


            <hr class="mb-5">

            <div class="concierge-button mx-auto">
                <a class="d-flex align-items-center justify-content-center" href="<?= base_url('servicios/concierge') ?>">    
                    <h2 class="">Concierge</h2>
                    <img class="concierge-icon mt-2" src="<?= base_url('assets/images/icons/icon-concierge.svg') ?>" alt="Concierge" height="25">
                </a>
            </div>


            <div class="d-flex align-items-center justify-content-center">
                <h4 class="mt-5">Si necesitas cualquier ayuda, nos ponemos a tus órdenes en el área de Concierge o llámanos al <a href="tel:3316194433">33.16.19.44.33</a></h4>
            </div>
            <div class="row my-3">
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_1.png'); ?>" alt="icon">
                    <p>Reservación para restaurantes o centros nocturnos.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_2.png'); ?>" alt="icon">
                    <p>Préstamo de Sillas de ruedas.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_3.png'); ?>" alt="icon">
                    <p>Préstamo de sombrillas.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <!-- <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_4.png'); ?>" alt="icon">
                    <p>Préstamo de cargadores para celular.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div> -->

                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_5.png'); ?>" alt="icon">
                    <p>Bolero.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_6.png'); ?>" alt="icon">
                    <p>Asistencia a visitantes extranjeros.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_7.png'); ?>" alt="icon">
                    <p>Servicio de lavado de autos.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_8.png'); ?>" alt="icon">
                    <p>Información de cartelera en cines.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>

                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_9.png'); ?>" alt="icon">
                    <p>Compra de boletos para cine.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_10.png'); ?>" alt="icon">
                    <p>Solicitud de servicio de taxi.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_11.png'); ?>" alt="icon">
                    <p>Guarda compras.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_12.png'); ?>" alt="icon">
                    <p>Wi-Fi.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>

                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_13.png'); ?>" alt="icon">
                    <p>Paramédico.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_14.png'); ?>" alt="icon">
                    <p>Asistente de compras personal.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_15.png'); ?>" alt="icon">
                    <p>Información turística.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div>
                <!-- <div class="service col-sm-12 col-md-6 col-lg-3 mt-4 mt-lg-5">
                    <img class="service-icon" src="<?= base_url('assets/images/services/icon_16.png'); ?>" alt="icon">
                    <p>Envío de compras a domicilio.</p>
                    <img class="mt-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
                </div> -->
            </div>
        </div>
    </section>



	<section class="title-section text-center">
		<div class="d-flex align-items-center justify-content-center">
			<h2>Novedades</h2>
			<span>NOVEDADES</span>
		</div>
		<br>
		<p class="mt-3">Conoce e infórmate de las novedades, promociones, eventos exclusivos y mucho más que tenemos para ti.</p>
		<img class="d-block mx-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
	</section>
	<section id="posts" class="pt-3">
		<div class="container">
			<div class="row my-3">
			<?php if (! empty($news) && is_array($news)) : ?>
				<?php $i = 0; shuffle($news); foreach ($news as $news_item): ?>
					<div class="col-sm-12 col-md-6 col-lg-4  mb-4 post-item">
						<img class="img-fluid w-100 mb-3" src="<?= base_url(esc($news_item['image'])); ?>" alt="News">
						<time>13 Septiembre 2020</time> 
						<h4 class="mt-3"><?= esc($news_item['title']); ?></span></h4>
						<img class="d-block mt-0 mb-4" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
						<p><?= esc($news_item['description']); ?></p>
					</div>	 
				<?php if (++$i == 3) break; endforeach; ?>
				<?php else : ?>
				<h3>No News</h3>
				<p>Unable to find any news for you.</p>
			<?php endif ?>	
			</div>		
		</div>
	</section>
<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>


<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>