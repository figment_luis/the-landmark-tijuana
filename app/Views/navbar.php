<nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light shadow px-md-0 px-sm-2">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url(); ?>" title="The Landmark Tijuana">
		<img src="<?= base_url('assets/images/landmark-mobile.svg'); ?>" alt="Logotipo The Landmark Tijuana"
             title="Logotipo The Landmark Tijuana" width="47" height="47" loading="lazy">
	</a>
    <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#nav-header" aria-controls="nav-header" aria-expanded="false" aria-label="Toggle navigation">
      <span class="my-1 mx-2 close">X</span>      
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-header">
      <div class="menu d-flex justify-content-end  flex-column flex-md-row text-dark text-center w-100">

      <?=  $this->include('options-menu') ?>

      <!--div class="d-block d-md-none concierge-menu-responsive pt-2">
          <a href="<?= base_url('servicios/concierge') ?>">    
              <p class="d-flex align-items-center">
                  SERVICIO DE CONCIERGE
                  <img src="<?= base_url('assets/images/icons/icon-concierge.svg') ?>" alt="Concierge" height="18" class="ml-2">
              </p>
          </a>
      </div-->
  
      </div>
    </div>
  </div>
</nav>