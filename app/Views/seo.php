<meta property="og:type" content="website"/>
<meta property="og:site_name" content="Visita The Landmark Tijuana Centro Comercial"/>
<meta property="og:locale" content="es-MX"/>
<meta property="og:title" content="Visita The Landmark Tijuana Centro Comercial"/>
<meta property="og:description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<meta property="og:url" content="<?= base_url() ?>"/>
<meta property="og:image" content="<?= base_url('assets/images/tijuana/seo/landmark.png')?>"/>

<meta name="robots" content="index, follow"/>
<link rel="canonical" href="https://thelandmarktijuana.com"/>

<meta property="article:publisher" content="https://www.facebook.com/TheLandmarkTijuana"/>
<meta property="article:author" content="https://www.facebook.com/TheLandmarkTijuana"/>

<meta itemprop="name" content="Visita The Landmark Tijuana Centro Comercial"/>
<meta itemprop="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<meta itemprop="url" content="<?= base_url() ?>"/>
<meta itemprop="image" content="<?= base_url('assets/images/tijuana/seo/landmark.png')?>"/>