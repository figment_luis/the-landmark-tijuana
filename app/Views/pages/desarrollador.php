<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Pent House<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= DESCRIPTION PAGE  ========= -->
<?= $this->section('description-page') ?>
<meta name="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->

<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/desarrollador.png")?><?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
<section class="information" id="info-page">
    <h3 class="information-title">Desarrollador</h3>
    <div class="information-subtitle">DESARROLLADOR</div>
    <img class="information-separator" src="<?= base_url('assets/images/line.svg')?>" alt="line" title="line" width="61" height="5" loading="lazy">
</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>
<section class="datasheet">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-md-6">
                <article class="datasheet-info">
                    <div class="datasheet-title">
                        <h4>NUESTRA ESENCIA</h4>
                        <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                    </div>
                    <div class="datasheet-body">
                        <p>The Landmark Tijuana es un innovador proyecto respaldado por dos de las más grandes firmas de desarrollo a nivel nacional e internacional.</p>
                        <img src="<?= base_url('assets/images/tijuana/logo-thorurbana.png')?>" alt="Logotipo Thor Urbana" class="img-fluid"
                             title="Logotipo Thor Urbana" width="393" height="72" loading="lazy">
                        <p>Thor Urbana Capital es una de las empresas de desarrollo e inversión inmobiliaria líderes en México. A través de una plataforma verticalmente integrada, se especializa en la búsqueda, adquisición, desarrollo, reposicionamiento, comercialización, administración y disposición de múltiples proyectos inmobiliarios incluyendo lifestyle centers, hoteles de lujo y proyectos de usos mixtos en las principales ciudades y destinos turísticos del país. La empresa actualmente desarrolla más de 750,000 m2 en distintos puntos del país como la Ciudad de México, Guadalajara, Playa del Carmen, Mérida, Metepec, Los Cabos, entre otros.</p>
                    </div>
                    <div class="datasheet-links datasheet-links--orange">
                        <a href="https://thorurbana.com" rel="noopener" rel="noreferrer" target="_blank" title="https://thorurbana.com">
                            thorurbana.com
                        </a>
                        <a href="https://www.thorurbana.com/portafolio" rel="noopener" rel="noreferrer" target="_blank" title="https://www.thorurbana.com/portafoliom">
                            Conoce más de nuestros desarrollos
                        </a>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-md-6 text-right">
                <img src="<?= base_url('assets/images/tijuana/nuestra-esencia.png')?>" alt="Nuestra esencia" class="img-fluid"
                     title="Nuestra esencia" width="535" height="695" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="datasheet last">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-md-6">
                <article class="datasheet-info">
                    <img src="<?= base_url('assets/images/tijuana/logo-gfa.png')?>" alt="Logotipo GFA" class="img-fluid datasheet-img-title"
                         title="Logotipo GFA" width="170" height="62" loading="lazy">
                    <div class="datasheet-body">
                        <p>Calidad y excelencia son sinónimos de GFA, una de las firmas desarrolladoras mexicana más importantes y sólidas de México.</p>
                        <p>Con una trayectoria que inició hace más de 50 años, ha sabido interpretar las necesidades y esencias de cada época con espacios inteligentes y funcionales que trascienden estilos y modas, incorporan visiones de diseño frescas y exploran nuevas posibilidades tecnológicas para integrarlas al estilo de vida de sus clientes y usuarios.</p>
                        <p>GFA ha destacado no sólo por su alto nivel de arquitectura y funcionalidad, sino también por la constante preferencia que el mercado ha mostrado por sus desarrollos residenciales, corporativos, turísticos e institucionales.</p>
                    </div>
                    <div class="datasheet-links">
                        <a href="https://gfa.com.mx" rel="noopener" rel="noreferrer" target="_blank" title="https://gfa.com.mx">
                            gfa.com.mx
                        </a>
                        <a href="https://www.gfa.com.mx/desarrollos/" rel="noopener" rel="noreferrer" target="_blank" title="https://www.gfa.com.mx/desarrollos/">
                            Conoce más de nuestros desarrollos
                        </a>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-md-6 text-right">
                <img src="<?= base_url('assets/images/tijuana/gfa.png')?>" alt="Instalaciones GFA" class="img-fluid"
                     title="Instalaciones GFA" width="537" height="705" loading="lazy">
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>


<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>


<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>
