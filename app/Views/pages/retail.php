<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Retail<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= DESCRIPTION PAGE  ========= -->
<?= $this->section('description-page') ?>
<meta name="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->

<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/shopping-and-work.png")?><?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
<section class="information" id="info-page">
    <h3 class="information-title">Retail</h3>
    <div class="information-subtitle">RETAIL</div>
    <img class="information-separator" src="<?= base_url('assets/images/line.svg')?>" alt="line" title="line" width="61" height="5" loading="lazy">
</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>
<section class="datasheet last">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-6 col-xl-6">
                <div class="datasheet-title">
                    <h4>SHOPPING</h4>
                    <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                </div>
                <div class="datasheet-body">
                    <p>El centro comercial que siempre has soñado.</p>
                    <p>The Landmark Tijuana es un estilo de vida cosmopolita de otro nivel, aquí encontrarás toda la sofisticación de marcas internacionales de moda y entretenimiento. También para tus gustos más exquisitos habrá una gran variedad de sabores gastronómicos.</p>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-5 offset-xl-1">
                <img src="<?= base_url('assets/images/tijuana/shopping.png') ?>" alt="Thumbnail Shopping" class="img-fluid datasheet-photo"
                     title="Thumbnail Shopping" width="674" height="450" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="datasheet last">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-6 col-xl-6">
                <div class="datasheet-title">
                    <h4>WORK</h4>
                    <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                </div>
                <div class="datasheet-body">
                    <p>Poder trabajar a unos pasos de tu hogar será una realidad.</p>
                    <p>The Landmark Tijuana contará con una torre de oficinas con espacios de trabajo únicos y flexibles, diseñados para maximizar la eficiencia en un entorno ameno, donde se estimule la creatividad y se cumplan tus metas.</p>
                    <p>Próximamente más información...</p>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-5 offset-xl-1">
                <img src="<?= base_url('assets/images/tijuana/work.png') ?>" alt="Thumbnail Work" class="img-fluid datasheet-photo"
                     title="Thumbnail Work" width="674" height="450" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="datasheet last">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-6 col-xl-6 d-none d-lg-block">
                <img src="<?= base_url('assets/images/tijuana/edificio.png')?>" alt="Edificio Thor Urbana" class="img-fluid datasheet-photo"
                     title="Edificio Thor Urbana" loading="lazy">
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-5 offset-xl-1">
                <form action="<?= base_url('/registro') ?>" method="post" class="form" id="contactForm">
                    <div class="form-group">
                        <label for="name">Nombre <span>*</span></label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico <span>*</span></label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="telephone">Teléfono <span>*</span></label>
                        <input type="text" name="telephone" id="telephone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="information_type">Me gustaría recibir más información de <span>*</span></label>
                        <select name="information_type" id="information_type" class="form-control">
                            <option value="" selected disabled>(...Agrega Recibir info...)</option>
                            <option value="corporativo">Corporativo</option>
                            <option value="residencial">Residencial</option>
                            <option value="comercial">Comercial</option>
                        </select>
                    </div>
                    <button type="submit" class="btn" id="btnSubmit">
                        <img src="<?= base_url('assets/images/tijuana/loader.svg') ?>" alt="loading" class="form-loading" id="loading"
                             title="loading" loading="lazy">
                        ENVIAR
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>


<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>


<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>
