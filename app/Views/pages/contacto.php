<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Contacto<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= DESCRIPTION PAGE  ========= -->
<?= $this->section('description-page') ?>
<meta name="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->

<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/contacto.png")?><?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
<section class="information" id="info-page">
    <h3 class="information-title">Contacto</h3>
    <div class="information-subtitle">CONTACTO</div>
    <img class="information-separator" src="<?= base_url('assets/images/line.svg')?>" alt="line" title="line" width="61" height="5" loading="lazy">
</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>
<section class="datasheet">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-6 col-xl-6">
                <article class="datasheet-info">
                    <div class="datasheet-body">
                        <a href="https://www.google.com.mx/maps/place/The+Landmark+Tijuana/@32.518753,-117.0202635,17z/data=!3m1!4b1!4m5!3m4!1s0x80d949cd98c66ccf:0xdddfe9acee4d7744!8m2!3d32.518753!4d-117.0180748?shorturl=1"
                           title="Ubicación The Landmark Tijuana" rel="noopener" rel="noreferrer" target="_blank">
                            <img src="<?= base_url('assets/images/maps.png') ?>" alt="Google Maps The Landmark Tijuana" class="img-fluid datasheet-map"
                                 title="Google Maps The Landmark Tijuana" width="660" height="485" loading="lazy">
                        </a>
                        <div class="datasheet-list-icons">
                            <img src="<?= base_url('assets/images/icons/icon-location.svg'); ?>" class="icon-direction"
                                 alt="Dirección: The Landmark Tijuana" title="Dirección: The Landmark Tijuana" width="31" height="41" loading="lazy">
                            <div>
                                <h4>DIRECCIÓN</h4>
                                <p>Blvd. Agua Caliente 10223, aviación Tijuana, BC, C.P. 22014</p>
                            </div>
                        </div>
                        <div class="datasheet-list-icons">
                            <img src="<?= base_url('assets/images/icons/icon-phone.svg'); ?>" class="icon-telephone" alt="Teléfono: The Landmark Tijuana"
                                 title="Teléfono: The Landmark Tijuana" width="24" height="40" loading="lazy">
                            <div>
                                <h5>TELÉFONO</h5>
                                <p><a href="tel:+526649068442" title="6649068442">T. (664) 906 8442</a></p>
                            </div>
                        </div>
                        <div class="datasheet-list-icons">
                            <img src="<?= base_url('assets/images/icons/icon-email.svg'); ?>" class="icon-mail" alt="Correo electrónico The Landmark Tijuana"
                                 title="Email The Landmark Tijuana" width="32" height="32" loading="lazy">
                            <div>
                                <h5>CORREO ELECTRÓNICO</h5>
                                <p class="mb-2"><a href="mailto:contacto@landmarktijuana.com" title="contacto@landmarktijuana.com">contacto@landmarktijuana.com</a></p>
                                <p><a href="mailto:leasing@landmarktijuana.com" title="leasing@landmarktijuana.com">leasing@landmarktijuana.com</a></p>
                            </div>
                        </div>
                        <div class="datasheet-list-icons--social">
                            <h5>SÍGUENOS EN NUESTRAS REDES SOCIALES</h5>
                            <div>
                                <a href="https://www.facebook.com/TheLandmarkTijuana" rel="noopener" rel="noreferrer" class="mr-3" target="_blank" title="Facebook"><i class="pt-4 fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/thelandmarktijuana/" rel="noopener" rel="noreferrer" class="mr-3" target="_blank" title="Instagram"><i class="pt-4 fa fa-instagram fa-2x" aria-hidden="true"></i></i></a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-5 offset-xl-1">
                <form action="<?= base_url('/registro') ?>" method="post" class="form" id="contactForm">
                    <div class="form-group">
                        <label for="name">Nombre <span>*</span></label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico <span>*</span></label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="telephone">Teléfono <span>*</span></label>
                        <input type="text" name="telephone" id="telephone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="information_type">Me gustaría recibir más información de <span>*</span></label>
                        <select name="information_type" id="information_type" class="form-control">
                            <option value="" selected disabled>(...Agrega Recibir info...)</option>
                            <option value="corporativo">Corporativo</option>
                            <option value="residencial">Residencial</option>
                            <option value="comercial">Comercial</option>
                        </select>
                    </div>
                    <button type="submit" class="btn" id="btnSubmit">
                        <img src="<?= base_url('assets/images/tijuana/loader.svg') ?>" alt="loading" title="Loading" class="form-loading"
                             id="loading" loading="lazy">
                        ENVIAR
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>


<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>


<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>

