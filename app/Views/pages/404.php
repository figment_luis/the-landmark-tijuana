<!DOCTYPE html>
<html lang="es-MX">
<head>
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>The Landmark Tijuana - Error 404</title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/assets/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/assets/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/assets/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/assets/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/assets/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/assets/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/assets/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/assets/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/assets/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>/assets/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/assets/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/assets/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/assets/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url() ?>/assets/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>/assets/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="author" type="text/plain" href="<?= base_url("humans.txt");?>" />
    <link rel="sitemap" type="application/xml" title="Sitemap" href="<?= base_url('sitemap.xml') ?>">
    <link rel="image_src" href="<?= base_url('assets/images/tijuana/seo/landmark.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/libs/bootstrap/bootstrap.min.css'); ?>" rel="preload">
    <link rel="stylesheet" href="<?= base_url('assets/css/fonts.css'); ?>" rel="preload">
    <link rel="stylesheet" href="<?= base_url("assets/css/style.css?v=".rand());?>">
    <style>
        * {
            border: 0;
            padding: 0;
            margin: 0;
            text-decoration: none;
            box-sizing: border-box;
            font-family: var(--main-font);
        }
        .container {
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }
        .image {
            display: block;
            margin-bottom: 1rem;
            max-width: 100%;
        }
        .message {
            color: #505050;
            font-size: 1.2rem;
            text-align: center;
            margin: 2.5rem 0 1.5rem;
        }
        .button {
            background-color: #FA3F83;
            padding: 10px 30px;
            color: white;
            display: inline-block;
            text-transform: uppercase;
            width: 190px;
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: var(--main-font);
        }
        .button:hover {
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<section class="container">
    <img src="<?= base_url('assets/favicons/logotipo.png') ?>" alt="Logotipo The Landmark Tijuana" title="The Landmark Tijuana" class="image" loading="lazy">
    <p class="message">Lo sentimos, el recurso solicitado ya no existe o posiblemente se ha movido a otra sección del sitio.</p>
    <a href="<?= base_url('/') ?>" title="<?= base_url('/') ?>" class="button">Ir al Home</a>
</section>
</body>

</html>