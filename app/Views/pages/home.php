<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Visita<?= $this->endSection() ?>

<!-- ========= DESCRIPTION PAGE  ========= -->
<?= $this->section('description-page') ?>
<meta name="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>main-header<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?= $this->section('image-header') ?><?= $this->endSection() ?>

<!-- ========= Popups Dinámicos ========= -->
<?= $this->section('popups') ?>
    <?= $this->include('partials/popups') ?>
<?= $this->endSection()?>


<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
	<section class="mycard" id="info-page">
		<div class="container">
            <div class="mycard-watermark">TIJUANA</div>
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 mycard-header">
					<img class="img-fluid d-none d-sm-block mycard-image" src="<?= base_url('assets/images/tijuana/info-home.png'); ?>"
                         alt="Thumbnail The Landmark Tijuana" title="Thumbnail The Landmark Tijuana" loading="lazy">
					<div class="mycard-brand">LANDMARK<br>TIJUANA</div>
				</div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div class="mycard-description">
                        <div class="mycard-title">Somos</div>
                        <p>Un desarrollo de usos mixtos donde podrás vivir, trabajar y disfrutar de tu vida, en un entorno diseñado para estimular todas tus experiencias de una forma nunca antes vista.</p>
                        <p>The Landmark Tijuana está conformado por una torre residencial, un centro comercial, una torre de oficinas y un hotel, todo este desarrollo diseñado bajo el concepto Lifestyle Retail Center, una idea única que consiste en conectar a sus habitantes en los diferentes espacios para una mayor interacción y comodidad.</p>
                        <p>Este proyecto de usos múltiples desarrollado en conjunto por Thor Urbana y GFA, cuenta con más de 30,000 m² de áreas comerciales, 14,000 m² de oficinas modernas AAA, 193 condominios y un hotel con más de 200 habitaciones.</p>
                    </div>
                </div>
			</div>
		</div>
	</section>
<?= $this->endSection() ?>



<!-- ========= CONTENT EXTRA  ========= -->

<?= $this->section('content-extra') ?>
<div class="mycard ">
    <div class="container">
        <div class="row mycard-background">
            <div class="col-md-12 col-lg-6 d-flex flex-column justify-content-center">
                <h4 class="mycard-title-alternative">UBICACIÓN</h4>
                <p>The Landmark Tijuana un lugar único ubicado donde fue Calette, la zona con mayor desarrollo y plusvalía de la ciudad.</p>
                <h4 class="mycard-title-alternative">EN EL CENTRO DE TODO</h4>
                <p>También conocida como Zona Dorada, es el centro de los servicios financieros de la ciudad y una de las zonas con mayor plusvalía de Tijuana debido a la gran variedad de servicios, centros comerciales, restaurantes y su gran crecimiento residencial de gran flujo.</p>
            </div>
            <div class="col-md-12 col-lg-6 d-none d-lg-block">
                <img src="<?= base_url('assets/images/tijuana/edificio.png')?>" alt="Edificio Thor Urbana" title="Edificio Thor Urbana" class="img-fluid" loading="lazy">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <hr>
</div>
<section class="mycard" id="info-page">
    <div class="container">
        <div class="row">
            <div class="mycard-watermark mycard-watermark--sm">THOR URBANA</div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 mycard-header">
                <img class="img-fluid d-none d-sm-block" src="<?= base_url('assets/images/tijuana/info-thorurbana.png'); ?>"
                     alt="Thumbnail Thor Urbana" title="Thumbnail Thor Urbana" loading="lazy">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                <div class="mycard-description">
                    <div class="mycard-title">Un proyecto de</div>
                    <img src="<?= base_url('assets/images/tijuana/logo-thorurbana.png') ?>" alt="Logotipo Thor Urbana"
                         title="Logotipo Thor Urbana" class="img-fluid mb-4" width="300" height="55" loading="lazy">
                    <p>Thor Urbana es una de las empresas de desarrollo e inversión inmobiliaria líderes en México. A través de una plataforma verticalmente integrada, se especializa en la búsqueda, adquisición, desarrollo, reposicionamiento, comercialización, operación, administración y disposición de múltiples proyectos inmobiliarios, incluyendo lifestyle centers, hoteles de lujo, proyectos de usos mixtos y parques industriales en las principales ciudades y destinos turísticos.</p>
                    <p>La empresa tiene desarrollos que suman más de 1.5 millones de metros cuadrados en distintos puntos del país y el exterior, como la Ciudad de México, Guadalajara, Playa del Carmen, Cancún, Mérida, Metepec, Los Cabos, Tulum, San Luis Potosí, Tijuana, Riviera Nayarit y Belice, entre otros.</p>
                    <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mycard" id="info-page">
    <div class="container">
        <div class="row">
            <div class="mycard-watermark mycard-watermark--sm">GFA MÉXICO</div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 mycard-header">
                <img class="img-fluid d-none d-sm-block" src="<?= base_url('assets/images/tijuana/info-architects.png'); ?>"
                     alt="Thumbnail Elkus Manfredi" title="Thumbnail Elkus Manfredi" loading="lazy">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 ">
                <div class="mycard-description">
                    <div class="mycard-title">Un proyecto de</div>
                    <img src="<?= base_url('assets/images/tijuana/logo-gfa.png')?>" alt="Logotipo GFA" class="img-fluid mb-4"
                         title="Logotipo GFA" width="170" height="62" loading="lazy">
                    <p>Calidad y excelencia son sinónimos de GFA, una de las firmas desarrolladoras mexicana más importantes y sólidas de México.</p>
                    <p>Con una trayectoria que inició hace más de 50 años, ha sabido interpretar las necesidades y esencias de cada época con espacios inteligentes y funcionales que trascienden estilos y modas, incorporan visiones de diseño frescas y exploran nuevas posibilidades tecnológicas para integrarlas al estilo de vida de sus clientes y usuarios.</p>
                    <p>GFA ha destacado no sólo por su alto nivel de arquitectura y funcionalidad, sino también por la constante preferencia que el mercado ha mostrado por sus desarrollos residenciales, corporativos, turísticos e institucionales.</p>
                    <div class="d-flex flex-column align-items-start mb-4 mycard-links">
                        <a href="https://gfa.com.mx" rel="noopener" rel="noreferrer" target="_blank" title="https://gfa.com.mx">
                            gfa.com.mx
                        </a>
                        <a href="https://www.gfa.com.mx/desarrollos/" rel="noopener" rel="noreferrer" target="_blank" title="https://www.gfa.com.mx/desarrollos/">
                            Conoce más de nuestros desarrollos
                        </a>
                    </div>


                    <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
	<link rel="stylesheet" href="<?= base_url('assets/css/promotion.css'); ?>" rel="preload">
    <link rel="stylesheet" href="<?= base_url('assets/css/popup.css'); ?>" rel="preload">
	<link rel="stylesheet" href="<?= base_url('assets/libs/fancybox/jquery.fancybox.min.css'); ?>" rel="preload">
	<link rel="stylesheet" href="<?= base_url('assets/css/events.css'); ?>" rel="preload">
<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
	<script src="<?= base_url('assets/js/events.js'); ?>" defer></script>
	<script src="<?= base_url('assets/libs/fancybox/jquery.fancybox.min.js'); ?>" defer></script>
	<script src="<?= base_url('assets/libs/moment/moment-with-locales.js'); ?>" defer></script>
	<script src="<?= base_url('assets/libs/moment/moment-timezone.min.js'); ?>" defer></script>
	<script src="<?= base_url('assets/libs/moment/moment-timezone-with-data.min.js'); ?>" defer></script>
    <!-- Lógica Popups Dinámicos -->
    <?php if ($popup != null && is_object($popup)): ?>
        <script>
            const current_time = moment().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");

            const date_open = '<?php echo $popup->start_date ?>';
            const date_close = '<?php echo $popup->end_date ?>';

            let popupvisible = false;
            let isMobile = false;

            // Verificar si el popup aun debe estar visible en el sitio
            if (current_time >= date_open && current_time <= date_close){
                popupvisible = true;
            }

            // Verificar si el sitio esta siendo consumido por algún dispositivo movil
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                isMobile = true;
            }

            // Mostrar el popup correspondiente [Desktop || Mobile]
            if(popupvisible){
                if(isMobile === false){
                    setTimeout(function(){
                               $.fancybox.open({
                                               src: '#popup_desktop'
                                               });
                               }, 1000);
                } else {
                    setTimeout(function(){
                               $.fancybox.open({
                                               src: '#popup_mobile'
                                               });
                               }, 1000);
                }
            }
        </script>
    <?php endif; ?>
<?= $this->endSection() ?>
