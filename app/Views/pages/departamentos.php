<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Departamentos<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= DESCRIPTION PAGE  ========= -->
<?= $this->section('description-page') ?>
<meta name="description" content="Ubicado en Blvd. Agua Caliente, The Landmark Tijuana es el centro comercial más exclusivo y con las mejores opciones de entretenimiento en Tijuana."/>
<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->

<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/live.png")?><?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
<section class="information" id="info-page">
    <h3 class="information-title">Departamentos</h3>
    <div class="information-subtitle">DEPARTAMENTOS</div>
    <img class="information-separator" src="<?= base_url('assets/images/line.svg')?>" alt="line" title="line" width="61" height="5" loading="lazy">
</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>
<section class="datasheet datasheet-mb--regular">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-5">
                <article class="datasheet-info">
                    <div class="datasheet-body">
                        <p>Sabemos que tu hogar es el lugar especial en donde puedes desarrollar, crear y descubrir tus propios límites. El espacio en donde la mejor colección de recuerdos, sonrisas y logros suceden.</p>
                        <p class="datasheet-text--decoration">Es por esto, que The Landmark Tijuana será el lugar perfecto para que puedas desarrollar la mejor versión de ti.</p>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 offset-lg-1 text-right">
                <img src="<?= base_url('assets/images/tijuana/live-somos.png')?>" alt="Un lugar especial" class="img-fluid"
                     title="Un lugar especial" width="674" height="450" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="datasheet datasheet-mb--regular">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-5">
                <article class="datasheet-info">
                    <div class="datasheet-title">
                        <h4>TODO LO QUE NECESITAS<br>EN UN SOLO LUGAR</h4>
                        <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                    </div>
                    <div class="datasheet-body">
                        <p>The Landmark Residences forma parte de un proyecto de usos mixtos que le permitirá a sus residentes experimentar un estilo de vida nunca antes visto en la ciudad. Bajo el concepto de The Landmark Club, la experiencia de vida se convierte en algo extraordinario.</p>
                        <ul class="datasheet-list--pink">
                            <li>Torre de 23 niveles</li>
                            <li>193 departamentos desde 59 m<sup>2</sup> hasta 139 m<sup>2</sup></li>
                            <li>4 PH de 230 m<sup>2</sup> y 260 m<sup>2</sup></li>
                            <li>Departamentos desde 1 a 3 recámaras</li>
                            <li>Más de 2,500 m<sup>2</sup> de todo tipo de amenidades</li>
                            <li>Seguridad, privacidad y servicios exclusivos para residentes</li>
                        </ul>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 offset-lg-1 text-right">
                <!-- TODO: Galería Slide - Zoom -->
                <div id="slick-live">
                    <?php foreach($slides as $slide) : ?>
                        <a href="<?= base_url($slide->image)?>" data-lightbox="live-<?= $slide->id ?>">
                            <img src="<?= base_url($slide->image_zoom)?>" alt="Galería 1" class="img-fluid" title="<?= esc($slide->title) ?>" width="674" height="450" loading="lazy">
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="datasheet datasheet-mb--regular">
    <div class="container">
        <div class="row padding">
            <div class="col-xs-12 col-lg-5">
                <article class="datasheet-info">
                    <div class="datasheet-title">
                        <h4>THE LANDMARK CLUB</h4>
                        <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                    </div>
                    <div class="datasheet-body">
                        <p>Hoy es posible disfrutar tu hogar al máximo, con más de 14 amenidades diferentes y servicios que complementen y lleven tu estilo de vida a otro nivel.</p>
                        <p>Aprovecharás tus días haciendo tus rutinas en el gimnasio, disfrutando un paseo por los jardines o relajándote en la alberca. Realizarás tus actividades favoritas disfrutando momentos inolvidables con tu familia y amigos.</p>
                        <p class="datashet-text--pink">Vivirás en forma extraordinaria.</p>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 offset-lg-1 text-right">
                <img src="<?= base_url('assets/images/tijuana/live-club.png')?>" alt="Thumbnail The Landmark Club" class="img-fluid"
                     title="Thumbnail The Landmark Club" width="674" height="450" loading="lazy">
            </div>
        </div>
    </div>
</section>
<div class="container">
    <hr>
</div>
<section class="datasheet datasheet-mt--regular">
    <div class="container">
        <div class="row">
            <div class="col-12 datasheet-title">
                <h4>AMENITIES PLAN</h4>
                <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy" loading="lazy">
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-6">
                <article class="datasheet-info">
                    <div class="datasheet-body">
                        <a href="<?= base_url('assets/images/tijuana/amenities-zoom.jpg')?>" data-lightbox="amenities-1" >
                            <img src="<?= base_url('assets/images/tijuana/amenities.png') ?>" alt="Thumbnail Amenities Plan" title="Thumbnail Amenities Plan"
                                 class="img-fluid datasheet-map datasheet-map-bg" width="829" height="578" loading="lazy">
                        </a>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-5 offset-xl-1">
                <div class="datasheet-title">
                    <h4>QUIERO RECIBIR INFORMACIÓN</h4>
                    <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                </div>
                <form action="<?= base_url('/registro') ?>" method="post" class="form" id="contactForm">
                    <div class="form-group">
                        <label for="name">Nombre <span>*</span></label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico <span>*</span></label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="telephone">Teléfono <span>*</span></label>
                        <input type="text" name="telephone" id="telephone" class="form-control">
                    </div>
                    <input type="hidden" name="information_type" value="residencial">
                    <button type="submit" class="btn" id="btnSubmit">
                        <img src="<?= base_url('assets/images/tijuana/loader.svg') ?>" alt="loading" class="form-loading" id="loading"
                             title="loading" loading="lazy">
                        ENVIAR
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="datasheet datasheet-mb--regular">
    <div class="container">
        <div class="row">
            <?php foreach ($plans as $plan): ?>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3">
                <article class="novelty">
                    <div class="novelty-container">
                        <img src="<?= base_url($plan->image) ?>" alt="Thumbnail <?= $plan->title ?>" title="Thumbnail <?= $plan->title ?>"
                             class="novelty-image" width="530" height="780" loading="lazy">
                        <p class="novelty-description"><?= $plan->description ?></p>
                    </div>
                    <h5 class="novelty-title"><?= $plan->title ?></h5>
                </article>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<div class="container">
    <hr>
</div>
<section class="datasheet datasheet-mt--regular">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-6 col-xl-5">
                <article class="datasheet-info">
                    <div class="datasheet-title">
                        <h4>THE LANDMARK EXPERIENCE</h4>
                        <img src="<?= base_url('assets/images/line.svg') ?>" alt="line" title="line" width="61" height="5" loading="lazy">
                    </div>
                    <div class="datasheet-body">
                        <p>La vanguardia en exclusividad siempre nos ha distinguido y hoy creamos <strong>The Landmark Experience</strong>, un plus para una vida práctica y cómoda.</p>
                        <p>The Landmark Experience son servicios que harán más feliz tu vida en tu nuevo hogar.</p>
                        <p>Las experiencias son opcionales y los podrás contratar con la administración de tu edificio.</p>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-lg-6 col-xl-6 offset-xl-1">
                <img src="<?= base_url('assets/images/tijuana/experience.jpg') ?>" alt="Thumbnail The Landmark Experience" title="Thumbnail The Landmark Experience"
                     class="img-fluid datasheet-map datasheet-map-bg" width="674" height="450" loading="lazy">
            </div>
        </div>
    </div>
</section>
<section class="datasheet datasheet-mb--regular">
    <div class="container">
        <div class="row">
            <?php foreach ($experiences as $experience): ?>
                <div class="col-12 col-sm-6 col-lg-4 col-xl-3">
                    <article class="novelty">
                        <div class="novelty-container">
                            <img src="<?= base_url($experience->image) ?>" alt="Thumbnail <?= $experience->title ?>" title="Thumbnail <?= $experience->title ?>"
                                 class="img-fluid novelty-image" width="530" height="780" loading="lazy">
                            <p class="novelty-description"><?= $experience->description ?></p>
                        </div>
                        <h5 class="novelty-title"><?= $experience->title ?></h5>
                    </article>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?= $this->endSection() ?>


<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>

