  <!--########################### Busqueda #######################################################################-->

    <!-- Modal -->
    <div class="modal fade" id="fullscreen-search" tabindex="-1" role="dialog" aria-labelledby="Search"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="search-close container pt-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                             < Regresar </span><!--Valor por dejecto "&times;"-->
                    </button>
                </div>
                <div class="text-center py-5">
                    <a href="<?= base_url() ?>">
                    <img src="<?= base_url('assets/images/landmark-black.svg'); ?>" alt="The Landmark Guadalajara"></a>  
                </div>   

                <div class="menu d-none d-md-flex justify-content-center  flex-column flex-md-row text-dark  text-center">
                        <?= $this->include('options-menu') ?>
                </div>

                
                <div class="modal-body search-content w-100">
                    <div class="container text-center">
                        <div class="modal-search-field d-flex justify-content-center mx-auto">
                            <div class="input-group mb-3 w-100 justify-content-center">
                                <input type="text" class="form-control text-center" placeholder="BUSCAR" aria-label="Buscar"
                                        aria-describedby="input-search" name ="search_text" id="search_text">
                            </div>
                        </div><!--Modal-search-field-->
                        <p>Las mejores tiendas, eventos y entretenimiento en un sólo lugar</p>
                    </div>
                </div>
                <!--Contenido Busqueda-->
                <!--El siguiente div servira como plantilla para los resultados de búsqueda-->
                <div id="search_result" class="result-search container">
                
                </div><!--result Busqueda  #scrollbar1-->
            </div><!--Modal Content-->
        </div>
    </div> <!--Modal-->

    <!--########################### Fin Busqueda ####################################################################-->
