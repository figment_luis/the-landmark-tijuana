<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?><?= ucfirst($title); ?><?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>side-header<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?php if(strtoupper($title) == "GASTRONOMIA"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/gastronomia.jpg")?><?= $this->endSection() ?>
<?php elseif(strtoupper($title) == "SHOPPING"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/shopping.jpg")?><?= $this->endSection() ?>
<?php elseif(strtoupper($title) == "ENTRETENIMIENTO"): ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/landmark_cinemex.jpg")?><?= $this->endSection() ?>
<?php else: ?>
	<?= $this->section('image-header') ?><?php echo base_url("assets/images/header/header-side.jpg")?><?= $this->endSection() ?>
<?php endif; ?>
<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
	<section class="pt-5 pb-5 position-relative info-page" id="info-page">
		<div class="container position-relative">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<img class="img-fluid" src="<?= base_url('assets/images/about-us.jpg'); ?>" alt="The Landmark Guadalajara">
					<span class="text-info-deco"><?= str_replace('GASTRONOMIA','GASTRONOMÍA',strtoupper($title)); ?></span>
				</div>
			</div>
			<?php if(strtoupper($title) == "SHOPPING"): ?>
			<div class="text-info-section col-sm-12">
				<span class="title-info">Compras</span>
				<p>The Landmark es su destino único para la mejor ropa, accesorios, belleza y más. Con su colección de tiendas de vanguardia y tiendas emergentes en constante cambio, The Landmark ofrece una experiencia de compra dinámica como ninguna otra en Guadalajara.
				<img class="d-block mt-3" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
				</p>
			</div>
			<?php elseif(strtoupper($title) == "GASTRONOMIA"): ?>
			<div class="text-info-section col-sm-12">
				<span class="title-info">Saborea</span>
				<p>Desde deliciosos cortes de carne hasta deliciosos macarons, sushi fresco, italiano al aire libre, estilo familiar y bistró romántico, sus opciones gastronómicas en The Landmark son tan diversas como sus antojos. Venga con hambre y salga feliz de uno de nuestros deliciosos restaurantes, abiertos para almuerzos al mediodía, happy hours después del trabajo, cenas nocturnas y comidas los fines de semana.
.
				<img class="d-block mt-3" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
				</p>
			</div>
			<?php elseif(strtoupper($title) == "ENTRETENIMIENTO"): ?>
			<div class="text-info-section col-sm-12">
				<span class="title-info">Grandes momentos</span>
				<p>Landmark Guadalajara está hecho para ser el hogar de experiencias increíbles. Dentro del centro comercial, tendrás vivencias únicas y crearás recuerdos que te acompañarán toda la vida. Lo único que tienes que hacer es venir y decidir dónde quieres empezar a escribir tu historia.
				<img class="d-block mt-3" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
				</p>
			</div>
			<?php else: ?>
			<div class="text-info-section col-sm-12">
				<span class="title-info">Diversas opciones</span>
				<p>The Landmark - Concierge ofrece a los visitantes lo mejor en servicio al cliente. Nuestros embajadores brindan asistencia y guían a los huéspedes a través de su experiencia de compra con las últimas ofertas de la tienda, direcciones, reservas y una variedad de servicios. Asegúrese de visitar Concierge, ubicado en el nivel inferior para recibir asistencia y consultas.
				<img class="d-block mt-3" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
				</p>
			</div>
			<?php endif; ?>


		</div>
		<div class="subtitle-info">
			<?= str_replace('GASTRONOMIA','GASTRONOMÍA',strtoupper($title)); ?>
		</div>
	</section>
<?= $this->endSection() ?>


<!-- ========= CONTENT EXTRA  ========= -->
<?= $this->section('content-extra') ?>

<!-- ========= FILTERS SubCategories  ========= -->
<?=  $this->include('filters') ?>

<section id="stores">
	<div class="container">
		<div class="row my-3">
			<?php if (! empty($stores) && is_array($stores)) : ?>
				<?php foreach ($stores as $store_item): ?>
					<div class="col-sm-12 col-md-6 col-lg-4  mb-4 item-store letter-<?= strtoupper(esc($store_item['name'][0])) ?>" data-letter="<?= strtoupper(esc($store_item['name'][0])) ?>">
						<?php
							$url = 'assets/images/stores/'.$store_item['id'].'/';
							$url_alternative = 'assets/images/stores/0/';
							if(is_dir($url)){
								$folder = opendir($url);
							}
							else{
								$folder = opendir($url_alternative);
								$value['id'] = 0;
							}
							$pic_types = array("jpg", "jpeg", "png");
							$images = array();
							while($file = readdir($folder))
							{
								if(in_array(substr(strtolower($file),strrpos($file,".") + 1),$pic_types))
								{
									array_push($images,$file);
								}
							}
							closedir($folder);
							sort($images);
						?>
						<a href="<?= base_url($title.'/'.esc($store_item['slug'])); ?>">
							<img class="img-fluid w-100 mb-3" src="<?= base_url(esc($url.$images[0])); ?>" alt="<?= esc($store_item['name']); ?>">
						</a>
						<a href="<?= base_url($title.'/'.esc($store_item['slug'])); ?>">
							<h4 class="mt-3 <?= esc($store_item['subcat_slug']); ?>"><?= esc($store_item['name']); ?></h4>
						</a>
						<img class="d-block mt-0 mb-4" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
					</div>
				<?php endforeach; ?>
			<?php else : ?>
				<h3>Sin resultados</h3>
				<p>No se han encontrado tiendas para este filtro</p>
			<?php endif ?>
        </div>
		<div id="notFindStores">
			<h3>Sin resultados</h3>
			<p>No se han encontrado tiendas para este filtro</p>
		</div>
	</div>
</section>


<section class="text-center mb-5 d-none">
	<button class="btn btn-mark">VER MÁS</button>
</section>


	<section class="title-section text-center">
		<div class="d-flex align-items-center justify-content-center">
			<h2>Novedades</h2>
			<span>NOVEDADES</span>
		</div>
		<br>
		<p class="mt-3">Conoce e infórmate de las novedades, promociones, eventos exclusivos y mucho más que tenemos para ti.</p>
		<img class="d-block mx-auto" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
	</section>
	<section id="posts" class="pt-3">
		<div class="container">
			<div class="row my-3">
			<?php if (! empty($news) && is_array($news)) : ?>
				<?php $i = 0; shuffle($news); foreach ($news as $news_item): ?>
					<div class="col-sm-12 col-md-6 col-lg-4  mb-4 post-item">
						<img class="img-fluid w-100 mb-3" src="<?= base_url(esc($news_item['image'])); ?>" alt="News">
						<time><?= $news_item['date']; ?></time>
						<h4 class="mt-3"><?= esc($news_item['title']); ?></span></h4>
						<img class="d-block mt-0 mb-4" src="<?= base_url('assets/images/line.svg'); ?>" alt="line">
						<p><?= esc($news_item['description']); ?></p>
					</div>
				<?php if (++$i == 3) break; endforeach; ?>
				<?php else : ?>
				<h3>Lo sentimos</h3>
				<p>No se encontraron novedades registradas en este momento, favor de intentar más tarde.</p>
			<?php endif ?>
			</div>
		</div>
	</section>
<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>


<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>

<?= $this->endSection() ?>
