<!doctype html>
<html lang="es-MX">
	<head>
		<!-- Google Tag Manager -->

		<!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- ========= SEO  ========= -->
        <title><?= $this->renderSection('title-page') ?> | The Landmark Tijuana Centro Comercial</title>
        <?= $this->renderSection('description-page') ?>
        <meta name="keywords" content="Tijuana, Landmark, Landmark Tijuana, Lifestyle Retail Center, GFA, The Landmark Tijuana, Thor, Thor Urbana, Grupo Thor Urbana, lifestyle centers, plazas comerciales, centros comerciales">
        <?= $this->include('seo.php') ?>
        <link rel="sitemap" type="application/xml" title="Sitemap" href="<?= base_url('sitemap.xml') ?>">
        <link rel="image_src" href="<?= base_url("assets/images/tijuana/seo/landmark.png")?>">
        <!-- ========= FAVICONS  ========= -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/assets/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/assets/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/assets/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/assets/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/assets/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/assets/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/assets/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/assets/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>/assets/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url() ?>/assets/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>/assets/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>/assets/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/assets/favicons/favicon-16x16.png">
        <link rel="manifest" href="<?= base_url() ?>/assets/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= base_url() ?>/assets/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- ========= AUTHOR  ========= -->
        <link rel="author" type="text/plain" href="<?= base_url("humans.txt");?>" />
        <!-- ========= STYLE SHEETS  ========= -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/bootstrap/normalize.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/libs/bootstrap/bootstrap.min.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/libs/animate/animate.min.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/fonts.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/style.css?v=1'); ?>" rel="preload">
        <link rel="stylesheet" href="<?= base_url('assets/css/tijuana.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/button-menu.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/responsive.css?v=1'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/modal-search.css'); ?>" rel="preload">
		<link rel="stylesheet" href="<?= base_url('assets/css/lightbox.css'); ?>" rel="preload">
        <link rel="stylesheet" href="<?= base_url('assets/libs/slick/slick.css') ?>" rel="preload">
        <link rel="stylesheet" href="<?= base_url('assets/libs/slick/slick-theme.css') ?>" rel="preload">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" rel="preload">
		<!-- ========= CSS EXTRA ========= -->
		<?= $this->renderSection('css_extra') ?>
		<script src="https://www.google.com/recaptcha/api.js"></script>
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->

		<!-- End Google Tag Manager (noscript) -->

        <!-- ========= Popups Dinámicos ========= -->
        <?= $this->renderSection('popups') ?>

		<!-- ========= NAVBAR  ========= -->
		<?= $this->include('navbar.php') ?>

		<!-- ========= HEADER  ========= -->
		<header class="position-relative header <?= $this->renderSection('type-header') ?>" style="background-image: url('<?= $this->renderSection('image-header')?>');">
			<?= $this->include('header.php') ?>
		</header>

		<!-- ========= INFO PAGE  ========= -->
		<?= $this->renderSection('info-page') ?>

		<!-- ========= CONTENT ========= -->
		<?= $this->renderSection('content-extra') ?>

		<!-- ========= NEWSLETEER  ========= -->
		<?php //echo $this->include('newsletter') ?>

		<!-- ========= FOOTER  ========= -->
		<?= $this->include('footer') ?>

		<!-- ========= MODALS  ========= -->
		<?php //echo $this->include('modal-search') ?>
		<?php //echo $this->include('modal-gallery') ?>

		<!-- ========= SCRIPTS  ========= -->
		<script src="<?= base_url('assets/libs/jquery/jquery-3.5.1.min.js') ?>" defer></script>
		<script src="<?= base_url('assets/libs/bootstrap/popper.min.js') ?>" defer></script>
		<script src="<?= base_url('assets/libs/bootstrap/bootstrap.min.js') ?>" defer></script>
        <script src="<?= base_url('assets/libs/slick/slick.min.js') ?>" defer></script>
        <script src="<?= base_url('assets/js/lightbox.js') ?>" defer></script>
        <script src="<?= base_url('assets/libs/sweetAlert/sweetalert2.all.min.js') ?>" defer></script>
		<script>let url = "<?= base_url(); ?>";</script>
		<script src="<?= base_url('assets/js/scripts.js'); ?>" defer></script>
        <script src="<?= base_url('assets/js/tijuana.js'); ?>" defer></script>
		<!-- ========= SCRIPTS EXTRA ========= -->
		<?= $this->renderSection('scripts-extra') ?>
		<script src="<?= base_url('assets/js/lightbox.js');?>" defer></script>
		<script>
			function onSubmit(token) {
				document.getElementById("newsletter-form").submit();
			}
		</script>
	</body>
</html>
