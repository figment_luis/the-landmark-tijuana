<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Aviso de privacidad<?= $this->endSection() ?>

<!-- ========= TYPE HEADER  ========= -->
<?= $this->section('type-header') ?>d-none<?= $this->endSection() ?>

<!-- ========= IMAGE HEADER  ========= -->
<?= $this->section('image-header') ?>assets/images/header/main.jpg<?= $this->endSection() ?>

<!-- ========= INFO PAGE  ========= -->
<?= $this->section('info-page') ?>
<section class="pt-5 pb-5">
		<div class="container position-relative">
            <h4>Aviso de privacidad</h4>
            <p><span style="font-weight: 400;">THOR URBANA CAPITAL, con domicilio en Paseo de la Reforma 2620, Piso 16, Colonia Lomas Altas, C.P. 11950, Ciudad de M&eacute;xico (en adelante &ldquo;TUC&rdquo;), reconoce la importancia que tiene el tratamiento leg&iacute;timo, controlado e informado de sus datos personales, por lo que en cumplimiento a la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares publicada en el Diario Oficial de la Federaci&oacute;n el d&iacute;a 5 de julio de 2010, su Reglamento publicado en el Diario Oficial de la Federaci&oacute;n el d&iacute;a 21 de diciembre de 2011y los Lineamientos del Aviso de Privacidad publicados en el Diario Oficial de la Federaci&oacute;n el 17 de enero de 2013 (la &ldquo;Ley&rdquo;), pone a su consideraci&oacute;n el presente Aviso de Privacidad.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">I. Datos personales que se tratar&aacute;n.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Algunos o todos de los siguientes datos personales ser&aacute;n tratados por TUC, ya que son necesarios para dar origen y lugar a la existencia, mantenimiento y cumplimiento de la relaci&oacute;n jur&iacute;dica entre Usted y TUC.</span></li>
            <li style="font-weight: 400;"><strong>Datos de identificaci&oacute;n:</strong><span style="font-weight: 400;"> nombre, domicilios, fecha de nacimiento, CURP, n&uacute;mero de c&eacute;dula de identificaci&oacute;n fiscal, n&uacute;mero de tel&eacute;fono, fijo o m&oacute;vil, fax, correo electr&oacute;nico u otros similares, as&iacute; como nombre de sus familiares y datos de contacto de estos.</span></li>
            <li style="font-weight: 400;"><strong>Datos acad&eacute;micos:</strong><span style="font-weight: 400;"> trayectoria educativa, t&iacute;tulo, n&uacute;mero de c&eacute;dula profesional, especialidad, certificados, filiaci&oacute;n a alguna asociaci&oacute;n, colegio o barra profesional, u otros similares.</span></li>
            <li style="font-weight: 400;"><strong>Datos laborales:</strong><span style="font-weight: 400;"> puesto, antig&uuml;edad laboral, record y antecedentes laborales, domicilio, correo electr&oacute;nico, n&uacute;mero de tel&eacute;fono, fijo o m&oacute;vil y fax, u otros similares.</span></li>
            <li style="font-weight: 400;"><strong>Datos financieros:</strong><span style="font-weight: 400;"> informaci&oacute;n de cuentas bancarias, estados financieros, forma de pago, u otros similares.</span></li>
            <li style="font-weight: 400;"><strong>Datos de facturaci&oacute;n:</strong><span style="font-weight: 400;"> domicilio fiscal, clave de identificaci&oacute;n fiscal y datos de la persona a quien ha de facturarse.</span></li>
            </ul>
            <p><span style="font-weight: 400;">Considerando la naturaleza de la relaci&oacute;n entre Usted y TUC, algunos de los datos se&ntilde;alados anteriormente y que ser&aacute;n recabados, pudieran involucrar bajo ciertas circunstancias, datos personales financieros o patrimoniales, los cuales ser&aacute;n tratados de conformidad con este Aviso de Privacidad.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">II. Datos Personales Sensibles.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Se hace de su conocimiento que TUC tratar&aacute; &uacute;nicamente aquellos datos personales sensibles que sean necesarios para la existencia, mantenimiento y cumplimiento de la relaci&oacute;n jur&iacute;dica entre Usted y TUC. Dichos datos personales sensibles podr&aacute;n ser: lesiones o accidentes, afiliaci&oacute;n a alg&uacute;n sindicato, afiliaciones sindicales o a organizaciones laborales y participaci&oacute;n en procesos o problemas judiciales.</span></li>
            </ul>
            <p><span style="font-weight: 400;">Considerando la naturaleza de la relaci&oacute;n entre Usted y TUC, algunos de los datos se&ntilde;alados anteriormente y que ser&aacute;n recabados, pudieran involucrar bajo ciertas circunstancias, datos personales financieros o patrimoniales, los cuales ser&aacute;n tratados de conformidad con este Aviso de Privacidad.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">III. Finalidades del tratamiento de los datos personales.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Los datos personales que nos proporcione voluntariamente, ser&aacute;n tratados, seg&uacute;n sea el caso, para la existencia, mantenimiento y cumplimiento de la relaci&oacute;n jur&iacute;dica entre Usted y TUC. TUC tratar&aacute; sus datos personales para:</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">1. Contactarlo;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">2. Crear y mantener actualizado su perfil en la base de datos de clientes;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Cumplir con el objeto de la relaci&oacute;n que dio origen al tratamiento de los datos personales;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">3. Para atender a sus preguntas y comentarios que nos haga llegar por cualquier medio;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">4. Enviarle informaci&oacute;n de inter&eacute;s;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">5. En general, para dar seguimiento a cualquier solicitud de servicios que se nos haga llegar.</span></li>
            </ul>
            <p><span style="font-weight: 400;">Se hace de su conocimiento que TUC no tratar&aacute; sus datos con fines de mercadotecnia, publicidad o prospecci&oacute;n comercial.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">IV. Transferencias de datos personales.</span></li>
            </ul>
            <p><span style="font-weight: 400;">TUC transferir&aacute; sus datos personales dentro del territorio nacional o al extranjero con la finalidad de dar lugar a la existencia, mantenimiento y cumplimiento de la relaci&oacute;n jur&iacute;dica entre Usted y TUC (los &ldquo;Receptores&rdquo;). TUC transferir&aacute; toda o parte de la informaci&oacute;n que Usted nos proporcione a los Receptores a manera de correos electr&oacute;nicos y/o en formas an&aacute;logas de transmisi&oacute;n de informaci&oacute;n.</span></p>
            <p><span style="font-weight: 400;">Los Receptores podr&aacute;n ser de dos tipos: (i) sociedades con las que TUC tiene una relaci&oacute;n contractual y/o comercial o profesional y (ii) terceros ajenos a TUC, cuando esta transferencia se haga precisamente para el mantenimiento o cumplimiento de la relaci&oacute;n jur&iacute;dica entre TUC y Usted, como podr&iacute;a ser de manera enunciativa, mas no limitativa, la transferencia de datos a asesores legales, fiscales, financieros o laborales, etc.</span></p>
            <p><span style="font-weight: 400;">En ambos casos, TUC har&aacute; conocer al receptor las medidas de protecci&oacute;n de datos personales implementadas por TUC. No obstante, TUC no responder&aacute; por el tratamiento que los terceros hagan de sus datos personales pues dichos actos est&aacute;n m&aacute;s all&aacute; del alcance de TUC. De igual manera, se le informa que TUC a su juicio, podr&aacute; solicitar informaci&oacute;n a terceros, respecto de sus antecedentes legales y personales, para lo cual, TUC verificar&aacute; que dichos transferentes cuenten con una pol&iacute;tica de privacidad que proteja sus datos personales.</span></p>
            <p><span style="font-weight: 400;">En cualquiera de los casos anteriores, se le informa que TUC no requiere de su consentimiento para realizar la transferencia de sus datos personales, en t&eacute;rminos del Art&iacute;culo 37 de la Ley. No obstante, en caso de que no est&eacute; de acuerdo con la transferencia de sus datos personales, favor de hacernos llegar un correo a info@thorurbana.com con el fin de que sus datos personales no sean transferidos. Cualquier otro tipo de transferencia de datos s&oacute;lo podr&aacute; hacerse siempre y cuando: (i) se encuentre prevista en la Ley, (ii) sea resultado de mandamiento de una autoridad competente, o (iii) se cuente con el consentimiento expreso del titular.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">V. Medidas de seguridad.</span></li>
            </ul>
            <p><span style="font-weight: 400;">TUC ha adoptado las medidas de seguridad, administrativas, t&eacute;cnicas y f&iacute;sicas, necesarias para proteger sus datos personales en contra de da&ntilde;o, p&eacute;rdida, alteraci&oacute;n, destrucci&oacute;n o el uso, acceso o tratamiento no autorizado. No obstante, dado que existen comunicaciones v&iacute;a internet fuera del alcance de nuestra vigilancia, no podemos garantizar en todo momento que sus datos personales transmitidos a nosotros por esa v&iacute;a, estar&aacute;n libres de uso, acceso o tratamiento no autorizado.</span></p>
            <p><span style="font-weight: 400;">Las medidas de seguridad adoptadas por TUC se revisar&aacute;n y examinar&aacute;n a la luz de las novedades t&eacute;cnicas y jur&iacute;dicas y no ser&aacute;n menores a aquellas que TUC adopte para proteger su propia informaci&oacute;n.</span></p>
            <p><span style="font-weight: 400;">Si tuvi&eacute;ramos conocimiento que ocurra una vulneraci&oacute;n de seguridad en cualquier fase del tratamiento de datos personales, que afecte de forma significativa sus derechos patrimoniales o morales, el titular del &aacute;rea de datos personales de TUC le comunicar&aacute; de forma inmediata por correo electr&oacute;nico el suceso de vulneraci&oacute;n de seguridad, para que Usted pueda tomar las medidas necesarias correspondientes para la defensa de sus derechos.</span></p>
            <p><span style="font-weight: 400;">En cualquier caso, el acceso a sus datos personales, en poder de TUC, se limitar&aacute; a las personas que necesiten tener acceso a dicha informaci&oacute;n, con el prop&oacute;sito de llevar a cabo las finalidades identificadas en este Aviso de Privacidad.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">VI. Derechos que le corresponden al titular de datos personales.</span></li>
            </ul>
            <p><span style="font-weight: 400;">Como titular de datos personales, Usted podr&aacute; dirigirse al Departamento de Datos Personales de TUC para ejercer los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y/u oposici&oacute;n (&ldquo;Derechos ARCO&rdquo;) establecidos en la Ley. Asimismo, puede revocar, en todo momento, el consentimiento que haya otorgado y que fuere necesario para el tratamiento de sus datos personales, as&iacute; como limitar el uso o divulgaci&oacute;n de los mismos. Si, con posterioridad a la revocaci&oacute;n, usted solicita la confirmaci&oacute;n de la misma, TUC le responder&aacute; de forma expresa.</span></p>
            <p><span style="font-weight: 400;">El ejercicio de los Derechos ARCO se har&aacute; mediante una solicitud por escrito dirigida el titular del &aacute;rea de datos personales de TUC, y enviada a la direcci&oacute;n de correo electr&oacute;nico info@thorurbana.com, o a nuestras oficinas ubicadas en Paseo de la Reforma 2620, Piso 16, Colonia Lomas Altas, C.P. 11950, Ciudad de M&eacute;xico, en un horario de lunes a viernes de 9:00 a 19:00 horas, con atenci&oacute;n al el titular del &aacute;rea de datos personales, manifestando detalladamente su solicitud (la &ldquo;Solicitud ARCO&rdquo;). La Solicitud ARCO deber&aacute; ir acompa&ntilde;ada de una copia de identificaci&oacute;n oficial vigente y se&ntilde;alar&aacute; el medio por el cual desea recibir respuesta por parte de TUC. El titular del &aacute;rea de datos personales de TUC responder&aacute; su Solicitud ARCO en el medio que usted haya elegido, manifestando los motivos de su decisi&oacute;n en un plazo m&aacute;ximo de 20 (veinte) d&iacute;as h&aacute;biles contados desde el d&iacute;a en que se haya recibido su Solicitud ARCO (o a partir del plazo que, en su caso, se establezca en Ley para tal efecto, lo que resulte mayor). El procedimiento se sustanciar&aacute; conforme a lo que se establece en la Ley.</span></p>
            <p><span style="font-weight: 400;">Para efectos de lo anterior, TUC pondr&aacute; a su disposici&oacute;n una serie de formatos sugeridos para llevar a cabo las solicitudes de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n, los cuales se encuentran disponibles directamente en el &aacute;rea de datos personales de TUC ubicado en las oficinas.</span></p>
            <p><span style="font-weight: 400;">En t&eacute;rminos de la Ley y su Reglamento, le informamos que, ante la negativa de respuesta a las solicitudes de derechos ARCO o inconformidad con la misma, Usted puede presentar ante el Instituto Nacional de Transparencia, Acceso a la Informaci&oacute;n y Protecci&oacute;n de Datos Personales, la correspondiente Solicitud ARCO en los plazos y t&eacute;rminos fijados por la Ley y su Reglamento.</span></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">VII. Cambios al aviso de privacidad</span></li>
            </ul>
            <p><span style="font-weight: 400;">Toda modificaci&oacute;n a este Aviso de Privacidad, se har&aacute; de su conocimiento por lo menos 30 (treinta) d&iacute;as antes a la fecha en que surtan efectos las modificaciones, por medio de la publicaci&oacute;n del aviso en la p&aacute;gina de internet www.thorurbana.com</span></p>
            <p><strong>&nbsp;</strong></p>
        </div>
    </section>
<?= $this->endSection() ?>

<!-- ========= CSS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
<?= $this->endSection() ?>

<!-- ========= SCRIPTS EXTRA  ========= -->
<?= $this->section('scripts-extra') ?>
<?= $this->endSection() ?>