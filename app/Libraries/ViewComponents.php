<?php namespace App\Libraries;
use App\Models\GetData;

class ViewComponents{

    public function recentPosts(array $params=[])
    {
        // $posts = $this->GetData->where('category', $params['category'])
        //                          ->orderBy('published_on', 'desc')
        //                          ->limit($params['limit'])
        //                          ->get();

        $posts = $this->GetData->findAll();
    
        return view('news', ['posts' => $posts]);
    }

}