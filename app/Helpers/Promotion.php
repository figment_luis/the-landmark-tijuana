<?php
namespace App\Helpers;

class Promotion
{
    // Datos de la promoción actual
    public $promo_name  = '';
    public $promo_start  = '2020-11-09 10:00:00';
    public $promo_end  = '2020-11-20 20:00:00';
    public $promo_bases_end  = '2020-11-31 23:59:59';
    // Images
    public $promo_banner_home  = '';
    public $promo_bases_bg_img  = '';
    public $promo_bases_ruta_images  = 'assets/images/promo/';
    public $promo_bases_prefijo_images  = 'bases_2020-';
    public $promo_bases_num_images_start  = 1;
    public $promo_bases_num_images_end  = 4;
    public $promo_bases_images_ext  = '.jpg';
    // Database
    public $promo_datatable  = '';
    public $promo_datatable_anterior  = '';
    //para el micrositio
    public $micrositio_name  = 'Promociones | The LandMark Guadalajara - Buen Fin 2020';
    public $micrositio_tracking  = '';
    public $micrositio_url  = 'buenfin';
    public $micrositio_landing  = 'assets/micrositio/buenfin2020/landing.jpg';
    public $micrositio_banner  = 'assets/images/popups/';
    public $popup_micrositio_desktop_img  = 'assets/micrositio/buenfin2020/desktop-popup.jpg';
    public $popup_micrositio_mobile_img  = 'assets/micrositio/buenfin2020/mobile-popup.jpg';
    // Type: popup (only popup), microsite(popup + link microsite)
    public $popup_micrositio_link  = 'popup';
}
